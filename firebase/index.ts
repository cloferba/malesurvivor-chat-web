import firebase from 'firebase/app'
import 'firebase/messaging'

const firebaseConfig = {
  apiKey: 'AIzaSyCLhuZ0fr_EBYLkTObXGGvPE1TZo7_GIbY',
  authDomain: 'malesurvivor-chat.firebaseapp.com',
  projectId: 'malesurvivor-chat',
  storageBucket: 'malesurvivor-chat.appspot.com',
  messagingSenderId: '208982389415',
  appId: '1:208982389415:web:13dbe7a090be288c5da182'
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}
export default firebase