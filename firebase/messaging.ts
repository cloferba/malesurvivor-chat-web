import socket from '../lib/socket'
import firebase from './'

export default async function initFCM() {
  try {
    const messaging = firebase.messaging()
    const vapidKey = 'BHpiYJpmXrdCvHUZRvGiTAQR_Pd022YyJ15cgKSjMFhKfI40_U9rmSBCWMiO5ZFZjPCe63xvtf9-u4H9c-xBpYE'
    const currentToken = await messaging.getToken({ vapidKey })
    if (currentToken) {
      socket.emit('set-fcm-token', currentToken)
    } else {
      // Show permission request UI
      console.log('No registration token available. Request permission to generate one.')
      // ...
    }
  } catch(err) {
    console.log('An error occurred while retrieving token. ', err)
  }
} 