import Head from 'next/head'
import UnauthorizedToast from '../components/UnauthorizedToast'
import TopBar from '../components/TopBar'
import { useRouter } from 'next/router'
import { MdArrowBack } from 'react-icons/md'
import IconButton from '../components/IconButton'
import { APP_NAME } from '../lib/constants'

interface AdminLayoutProps {
  children: any,
  back?: string,
  title: string,
  unauthorized?: boolean,
  extra?: any
}

export default function AdminLayout({children, back, title, unauthorized, extra}: AdminLayoutProps) {

  const router = useRouter()
  const backClicked = () => router.push(back)

  const appHeader = (
    <div className="flex items-center mb-4">
      {back ? (
        <div className="mr-2 relative top-0.5">
          <IconButton
            icon={MdArrowBack}
            onClick={backClicked}
          />
        </div>
      ) : null }
      <h3>
        {title}
      </h3>
      <div className="ml-auto">
        {extra}
      </div>
    </div>
  )

  return (
    <>
      <Head>
        <title>Admin - {title} - {APP_NAME}</title>
      </Head>
      <div className="h-full grid grid-rows-max-c-auto">
        <TopBar title="Admin panel" darkMode/>
        <div className="px-4 py-8 md:px-8 overflow-y-auto">
          <div className="max-w-3xl">
            {appHeader}
            {!unauthorized ? children : <UnauthorizedToast/>}
          </div>
        </div>
      </div>
    </>
  )
}