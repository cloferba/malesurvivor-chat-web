import { useRecoilValue } from 'recoil'
import userState from '../atoms/user'
import StatusSelector from '../components/StatusSelector'
import { User } from '../lib/types'
import UserRooms from './UserRooms'
import AdminTabs from './AdminTabs'
import Avatar from '../components/Avatar'
import useScrollQuery from '../hooks/useScrollQuery'
import { useRef } from 'react'
import Link from 'next/link'
import { FORUM_NAME, FORUM_URL } from '../lib/constants'
import { MdOpenInNew, MdPalette, MdDisabledVisible, MdSettings } from 'react-icons/md'
import clsx from 'clsx'
import IconButton from '../components/IconButton'
import Dropdown from '../components/Dropdown'

const defaultTheme = process.env.NEXT_PUBLIC_DEFAULT_THEME

const linkToForum = (
  <Link href={FORUM_URL}>
    <a
      target="_blank"
      className="link ml-2 h-10 inline-flex items-center"
      style={{color: 'white'}}
    >
      {FORUM_NAME} <MdOpenInNew className="ml-2" />
    </a>
  </Link>
)

const containerClass = clsx(
  'h-full grid grid-rows-max-c-auto-max-c overflow-hidden',
  defaultTheme ? 'bg-gray-100' : 'bg-forum-blue dark:bg-forum-dark-blue text-white'
)

const hrClass = clsx('my-5', defaultTheme ? null : 'opacity-30')

export default function Sidebar({
  openThemeSelector,
  openIgnoredUsers,
}) {

  const user = useRecoilValue<User>(userState)
  const contentRef = useRef(null)
  const scrolled = useScrollQuery(50, contentRef.current)

  if(!user) return null

  const settingsMenu = (
    <div className=" bg-gray-800 w-44 rounded overflow-hidden shadow-md relative -top-12 left-2">
      <div
        className="menu-item px-3 py-1.5 hover:bg-gray-700"
        onClick={openThemeSelector}
      >
        <MdPalette />
        <span>Theme</span>
      </div>
      <div
        className="menu-item px-3 py-1.5 hover:bg-gray-700"
        onClick={openIgnoredUsers}
      >
        <MdDisabledVisible />
        <span>Ignored users</span>
      </div>
    </div>
  )

  const header = (
    <div className={`flex items-center px-4 py-5 transition-shadow z-10 ${scrolled ? 'shadow-lg': ''}`}>
      <div className="mr-3 flex-shrink-0">
        <Avatar size="large" src={user.avatar_url_m} />
      </div>
      <div>
        <h3 className="mb-1 word-break">
          {user.username}
        </h3>
        <StatusSelector showOffline={user.permissions.useOfflineStatus}/>
      </div>
    </div>
  )

  const adminLinks = user.permissions.adminPanelAccess ? (
    <>
      <hr className={hrClass} />
      <h4 className="px-3 py-2">
        Admin links
      </h4>
      <AdminTabs />
    </>
  ) : null

  const settingsButton = (
    <Dropdown
      overlay={settingsMenu}
      placement="bottom"
    >
      <IconButton icon={MdSettings}/>
    </Dropdown>
  )

  return (
    <div className={containerClass}>
      {header}
      <div
        className="overflow-y-auto"
        ref={contentRef}
      >
        {adminLinks}
        <hr className={hrClass} />
        <UserRooms/>
        <p className="py-8 md:py-0" />
      </div>
      <div className="flex items-center pb-1 px-1">
        {settingsButton}
        {linkToForum}
      </div>
    </div>
  )
}