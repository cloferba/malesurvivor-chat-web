import { MdPublic, MdPerson, MdAdd, MdVpnLock } from 'react-icons/md'
import RoomsList from '../components/RoomsList'
import { Room } from '../lib/types'
import { useRecoilValue } from 'recoil'
import roomsState from '../atoms/rooms'
import { useModal } from '../components/Modal'
import PublicRoomsList from './RoomsList'
import UsersList from './Room/usersList'
import clsx from 'clsx'
import UserRoomsTitle from './UserRoomsTitle'
import useFetchData from '../hooks/useFetchData'

const defaultTheme = process.env.NEXT_PUBLIC_DEFAULT_THEME

function ExploreRoomsButton({ label, onClick }) {
  return (
    <div className="px-3 mt-4">
      <button
        className="btn bg-accent text-white flex items-center"
        onClick={onClick}
      >
        <MdAdd className="mr-2" />
        {label}
      </button>
    </div>
  )
}

const hrClass = clsx('my-5', defaultTheme ? null : 'opacity-30')

const getHiddenRoomsPerType = rooms => rooms.reduce((acc, room) => {
  if (room.hidden) {
    acc[room.type] = true
  }
  return acc
}, {})

export default function UserRooms() {

  const { data: users } = useFetchData('get-app-users', {})
  const rooms = useRecoilValue(roomsState)
  const roomsArr: Room[] = Object.values(rooms)
  const publicRooms = roomsArr.filter(({type}) => type == 'public')
  const u2uRooms = roomsArr.filter(({type}) => type == 'u2u')
  const staffRooms = roomsArr.filter(({type}) => type == 'staff')
  const linkOnlyRooms = roomsArr.filter(({type}) => type == 'link-only')
  const { modal, closeModal } = useModal()
  const hiddenRoomsType = getHiddenRoomsPerType(roomsArr)

  function openPublicRoomsModal() {
    const desc = (
      <div className="mt-3">
        <PublicRoomsList onEnterRoomClicked={closeModal}/>
      </div>
    )
    modal({ title: 'Explore rooms', desc, hideDefaultButton: true })
  }

  function openAppUsersModal() {
    const desc = (
      <div className="mt-3">
        <UsersList
          users={users}
          onMdClicked={closeModal}
        />
      </div>
    )
    modal({ title: 'New chat', desc, hideDefaultButton: true, height: 400 })
  }

  const showPrivateRooms = Boolean(staffRooms.length) || Boolean(linkOnlyRooms.length)

  return (
    <>
      <UserRoomsTitle
        title="Public rooms"
        hasHiddenRooms={hiddenRoomsType.public}
      >
        {showHidden => (
          <RoomsList
            rooms={publicRooms}
            icon={MdPublic}
            showHidden={showHidden}
          />
        )}
      </UserRoomsTitle>
      <ExploreRoomsButton
        label="Explore rooms"
        onClick={openPublicRoomsModal}
      />
      <hr className={hrClass} />
      {showPrivateRooms && (
        <>
          <UserRoomsTitle
            title="Private rooms"
            hasHiddenRooms={hiddenRoomsType.staff || hiddenRoomsType['link-only']}
          >
            {showHidden => (
              <>
                <RoomsList
                  rooms={staffRooms}
                  icon={MdVpnLock}
                  showHidden={showHidden}
                />
                <RoomsList
                  rooms={linkOnlyRooms}
                  icon={MdVpnLock}
                  showHidden={showHidden}
                />
              </>
            )}
          </UserRoomsTitle>
          <hr className={hrClass} />
        </>
      )}
      <UserRoomsTitle
        title="Direct messages"
        hasHiddenRooms={hiddenRoomsType.u2u}
      >
        {showHidden => (
          <RoomsList
            rooms={u2uRooms}
            icon={MdPerson}
            showHidden={showHidden}
          />
        )}
      </UserRoomsTitle>
      <ExploreRoomsButton
        label="New chat"
        onClick={openAppUsersModal}
      />
    </>
  )
}