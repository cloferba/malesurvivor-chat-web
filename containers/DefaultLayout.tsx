import { useEffect, useState } from 'react'
import Drawer from '../components/Drawer'
import useIsSmallScreen from '../hooks/useIsSmallScreen'
import LayoutContext from './layoutContext'
import Sidebar from './Sidebar'
import { use100vh } from 'react-div-100vh'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import roomsState from '../atoms/rooms'
import socket from '../lib/socket'
import { useRouter } from 'next/router'
import connectedState from '../atoms/connected'
import { useModal } from '../components/Modal'
import ThemeSelector from '../components/ThemeSelector'
import IgnoredUsers from '../components/IgnoredUsers'
import setTheme from '../lib/setTheme'
import useSound from 'use-sound'

export default function DefaultLayout({children}) {

  const height = use100vh()
  const [menuVisible, setMenuVisible] = useState(false)
  const isSmallScreen = useIsSmallScreen()
  const { modal } = useModal()
  const [playNewMessage] = useSound('/new-message.wav')

  const setRooms = useSetRecoilState(roomsState)
  const connected = useRecoilValue(connectedState)

  useEffect(setTheme, [])

  useEffect(() => {
    if(connected) {
      socket.emit('get-user-rooms', ({data}) => setRooms(data))
    }
  }, [connected])

  const router = useRouter()
  
  useEffect(() => {
    socket.on('user/add-room', room => {
      setRooms(currentRooms => ({
        ...currentRooms,
        [room.id]: room,
      }))
    })

    socket.on('user/add-room-notification', roomId => {
      playNewMessage()
      setRooms(currentRooms => ({
        ...currentRooms,
        [roomId]: {
          ...currentRooms[roomId],
          hasNotification: true
        }
      }))
    })

    socket.on('room/update-count', ({ roomId, count }) => {
      setRooms(currentRooms => ({
        ...currentRooms,
        [roomId]: {
          ...currentRooms[roomId],
          onlineUsersCount: count
        },
      }))
    })

    const handleRouteChange = () => setMenuVisible(false)

    router.events.on('routeChangeStart', handleRouteChange)

    return () => {
      socket.off('user/add-room-notification')
      socket.off('user/add-room')
      socket.off('app-users/update')
      router.events.off('routeChangeStart', handleRouteChange)
    }
  }, [playNewMessage])

  const openThemeSelector = () => {
    const desc = <ThemeSelector />
    const title = 'Choose theme'
    modal({ desc, title, hideDefaultButton: true })
  }

  const openIgnoredUsers = () => {
    const desc = <IgnoredUsers />
    const title = 'Ignored users'
    modal({ desc, title, hideDefaultButton: true })
  }

  const sidebar = (
    <Sidebar
      openThemeSelector={openThemeSelector}
      openIgnoredUsers={openIgnoredUsers}
    />
  )
  
  return (
    <div
      style={{ height: height - 1 }}
      className="flex dark:bg-gray-900 dark:text-white"
    >
      {isSmallScreen ? (
        <Drawer
          visible={menuVisible}
          onClose={() => setMenuVisible(false)}
        >
          {sidebar}
        </Drawer>
      ) : (
        <div className="w-64 hidden lg:block">
          {sidebar}
        </div>
      )}
      <LayoutContext.Provider value={{ setMenuVisible }}>
        <main className="flex-1">
          {children}
        </main>
      </LayoutContext.Provider>
    </div>
  )
}