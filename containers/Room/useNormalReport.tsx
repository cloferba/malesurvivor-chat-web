import socket from '../../lib/socket'
import useConfirmModal from '../../components/ConfirmModal'
import { useModal } from '../../components/Modal'

export default function useNormalReport(messages) {

  const { openConfirm, closeConfirm } = useConfirmModal()
  const { modal } = useModal()

  const reportMessage = messageId => {
    socket.emit('report-message', messageId, r => {
      closeConfirm()
      const title = r.error ? r.errorMessage : 'Message reported'
      modal({ title })
    })
  }

  const handleReportClicked = messageIndex => {
    const message = messages[messageIndex]
    const title = 'Report message'
    const desc = `Are you sure you want to report this message from ${message.username}?`
    const onConfirm = () => reportMessage(message.id)
    openConfirm({title, desc, confirmText: 'Report', onConfirm, waitAfterConfirm: true })
  }

  return handleReportClicked
}