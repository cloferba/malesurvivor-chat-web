import { useModal } from '../../components/Modal'
import { User } from '../../lib/types'
import socket from '../../lib/socket'
import { useSetRecoilState } from 'recoil'
import userState from '../../atoms/user'

const IgnoreUser = ({ userId, closeModal }) => {

  const setUser = useSetRecoilState(userState)
  
  const confirmIgnore = () => {
    socket.emit('ignore-user', userId, ignored_users => {
      setUser(currUser => ({ ...currUser, ignored_users }))
      closeModal()
    })
  }

  return (
    <div className="mt-3">
      <p className="opacity-60 text-center">
        You will not see or receive messages of this user 
      </p>
      <div className="mt-7 text-right">
        <button
          className="btn btn-primary text-right"
          onClick={confirmIgnore}
        >
          Confirm
        </button>
      </div>
    </div>
  )
}

export default function useIgnoreUser() {
  const { modal, closeModal } = useModal()
  
  return (user: User) => {

    const desc = (
      <IgnoreUser
        userId={user.user_id}
        closeModal={closeModal}
      />
    )

    modal({
      title: `Ignore ${user.username}?`,
      desc,
      hideDefaultButton: true
    })
  }
}