import socket from '../../lib/socket'
import useConfirmModal from '../../components/ConfirmModal'

export default function useCensorMessageModal(messages) {
  
  const { openConfirm, closeConfirm } = useConfirmModal()

  const adminCensorMessage = messageId => {
    socket.emit('admin-censor-message', messageId, closeConfirm)
  }

  const handleCensorMessage = messageIndex => {
    const message = messages[messageIndex]
    const title = 'Censor message'
    const desc = `Censor this message from ${message.username}?`
    const onConfirm = () => adminCensorMessage(message.id)
    openConfirm({title, desc, confirmText: 'Yes, censor message', onConfirm, waitAfterConfirm: true })
  }

  return handleCensorMessage
}