import { useMemo } from 'react'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useRecoilValue, useRecoilState } from 'recoil'
import { MdDirectionsWalk, MdExitToApp, MdPeople } from 'react-icons/md'
import socket from '../../lib/socket'
import connectedState from '../../atoms/connected'
import Chat from './Chat'
import UsersList from './usersList'
import { User } from '../../lib/types'
import TopBar from '../../components/TopBar'
import Drawer from '../../components/Drawer'
import useIsSmallScreen from '../../hooks/useIsSmallScreen'
import roomsState from '../../atoms/rooms'
import IconButton from '../../components/IconButton'
import useSound from 'use-sound'

interface Router {
  query: {
    roomId?: string
  }
}

export default function Room() {

  const [rooms, setRooms] = useRecoilState(roomsState)
  const [roomUsers, setRoomUsers] = useState<Record<User['user_id'], User>>({})
  const router = useRouter()
  const { query: { roomId } }: Partial<Router> = router
  const connected = useRecoilValue(connectedState)
  const [connectedToRoom, setConnectedToRoom] = useState(false)
  const [usersDrawerVisible, setUsersDrawerVisible] = useState(false)
  const isSmallScreen = useIsSmallScreen()
  const room = roomId ? rooms[roomId] || {} : {}
  const [playNewUser] = useSound('/new-user.wav')
  const [playUserLeave] = useSound('/user-leave.wav')

  function removeRoomNotification() {
    if(!room.hasNotification) return
    setRooms(currentRooms => ({
      ...currentRooms,
      [roomId]: {
        ...currentRooms[roomId],
        hasNotification: false
      }
    }))
  }

  useEffect(() => {
    setConnectedToRoom(false)
    if(connected) {
      socket.emit('join-room', roomId, ({error, data}) => {
        if(error) router.push('/')
        if(data) {
          setRoomUsers(data)
          setConnectedToRoom(true)
        }
      })
    }
  }, [connected, roomId])

  useEffect(() => {
    removeRoomNotification()
  }, [room.hasNotification])
  
  useEffect(() => {
    socket.on('room/add-user', user => {
      playNewUser()
      setRoomUsers(currentUsers => ({
        ...currentUsers,
        [user.user_id]: user
      }))
    })

    socket.on('room/remove-user', userId => {
      playUserLeave()
      setRoomUsers(currentUsers => {
        let result = { ...currentUsers }
        delete result[userId]
        return result
      })
    })

    socket.on('contact/update', updateObj => {
      if (updateObj.status == 'offline') {
        // playUserLeave()
      }
      setRoomUsers(currentUsers => {
        const targetUser = currentUsers[updateObj.user_id]
        if(!targetUser) return currentUsers
        return {
          ...currentUsers,
          [updateObj.user_id]: { ...targetUser, ...updateObj }
        }
      })
    })

    socket.on('user/kick-out', () => router.push('/'))

    return () => {
      socket.off('room/add-user')
      socket.off('room/remove-user')
      socket.off('contact/update')
      socket.off('user/kick-out')
    }
  }, [playNewUser, playUserLeave])

  const showUsersList = room.type && room.type != 'u2u'

  const openUsersList = showUsersList && (
    <IconButton
      key="show_users_list"
      className="block lg:hidden"
      onClick={() => setUsersDrawerVisible(true)}
      icon={MdPeople}
    />
  )

  const exitClick = () => {
    window.location.href = 'https://google.com'
  }

  const leaveAndDelete = (
    <IconButton
      key="leave_and_delete"
      className="text-gray-800 dark:text-white"
      icon={MdExitToApp}
      onClick={() => {
        socket.emit('leave-room-and-delete', r => {
          setRooms(r.data)
        })
      }}
    />
  )

  const exitButton = (
    <IconButton
      key="exit_button"
      className="text-red-700"
      onClick={exitClick}
      icon={MdDirectionsWalk}
    />
  )

  const topBarExtra = [
    exitButton,
    openUsersList
  ]

  const topBar = (
    <TopBar
      leftExtra={leaveAndDelete}
      title={room.name}
      extra={topBarExtra}
    />
  )

  const onlineUsers = useMemo(() => {
    return Object.keys(roomUsers).reduce((acc, user_id) => {
      if(roomUsers[user_id].status != 'offline') {
        acc[user_id] = roomUsers[user_id]
      }
      return acc
    }, {})
  }, [roomUsers])

  const rightBar = (
    <div className="bg-gray-100 dark:bg-gray-800 h-full grid grid-rows-max-c-auto">
      <h4 className="px-4 py-3">
        Room's users ({Object.keys(onlineUsers).length})
      </h4>
      <div className="overflow-y-auto pt-1">
        <UsersList users={onlineUsers} />
      </div>
    </div>
  )

  return (
    <div className="h-full flex w-full">
      <div className="flex-1">
        { connectedToRoom && (
          <Chat {...{roomUsers, room, topBar }} />
        )}
      </div>
      {showUsersList && (
        isSmallScreen ? (
          <Drawer
            visible={usersDrawerVisible}
            onClose={() => setUsersDrawerVisible(false)}
            placement="right"
          >
            {rightBar}
          </Drawer>
        ) : (
          <div className="w-72 hidden lg:block">
            {rightBar}
          </div>
        )
      )}
    </div>
  )
}