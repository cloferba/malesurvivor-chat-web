import BanUser from '../../components/BanUser'
import { useModal } from '../../components/Modal'
import { User } from '../../lib/types'

export default function useBanUser() {
  const { modal, closeModal } = useModal()
  
  return (user: User) => {

    const desc = (
      <BanUser
        userId={user.user_id}
        closeModal={closeModal}
      />
    )

    modal({
      title: `Ban ${user.username}?`,
      desc,
      hideDefaultButton: true
    })
  }
}