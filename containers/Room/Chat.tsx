import { useEffect, useRef, useState } from 'react'
import socket from '../../lib/socket'
import Message from '../../components/Message'
import ChatInput from '../../components/ChatInput'
import ChatMessages from '../../components/ChatMessages'
import ActionsBox from '../../components/Message/ActionsBox'
import { useRecoilValue } from 'recoil'
import userState from '../../atoms/user'
import { Room, User } from '../../lib/types'
import useCreateReportModal from './useCreateReportModal'
import useCensorMessageModal from './useCensorMessageModal'
import useBanUser from './useBanUser'
import useIgnoreUser from './useIgnoreUser'
import getIsRoomIgnored from '../../lib/getIsRoomIgnored'
import IgnoringUserToast from '../../components/IgnoringUserToast'
import useSound from 'use-sound'

interface ChatProps {
  room?: Room,
  roomUsers?: Record<User['user_id'], User>,
  topBar?: any,
  hideActionsBox?: boolean
}

export default function Chat({ room, topBar, roomUsers, hideActionsBox }: ChatProps) {

  const [messages, setMessages] = useState([])
  const messagesRef = useRef(null)
  const [replyingTo, setReplyingTo] = useState(null)
  const user = useRecoilValue<User>(userState)
  const { id: roomId } = room
  const handleCreateReport = useCreateReportModal(messages)
  const handleCensorMessage = useCensorMessageModal(messages)
  const handleBanUser = useBanUser()
  const handleIgnoreUser = useIgnoreUser()
  const { isIgnoring, receiverId } = getIsRoomIgnored(user, room.users)
  const [playNewMessage] = useSound('/new-message.wav')

  const { censorMessages, createReports, banUser } = user.permissions

  const scrollBottom = () => {
    messagesRef.current.scroll({ top: messagesRef.current.scrollHeight })
  }

  const addMessage = message => {
    if (message.user_id != user.user_id) {
      playNewMessage()
    }
    setMessages(currentMessages => (
      [...currentMessages, message]
    ))
    scrollBottom()
  }

  const censorMessage = messageId => {
    const messageIndex = messages.findIndex(({id}) => id == messageId)
    if(messageIndex == -1) return
    
    setMessages(currentMessages => {
      let newMessages = [...currentMessages]
      newMessages[messageIndex].text = 'System Removed Content'
      newMessages[messageIndex].censored = true
      return newMessages
    })
  }

  useEffect(() => {
    socket.on('new-message', addMessage)
    socket.on('censor-message', censorMessage)
    return () => {
      socket.off('new-message')
      socket.off('censor-message')
    }
  }, [messages.length])

  useEffect(() => {
    socket.emit('get-last-messages', lastMessages => {
      if(!messagesRef.current) return
      setMessages(lastMessages)
      scrollBottom()
    })
  }, [])

  const getPrevious = cb => {
    if(!roomId) return
    socket.emit('get-previous-messages', roomId, messages[0].id, previousMessages => {
      setMessages(currentMessages => [...previousMessages, ...currentMessages])
      cb()
    })
  }

  const handleReplayClicked = messageIndex => {
    setReplyingTo(messages[messageIndex])
  }

  const sendMessage = (text) => {
    socket.emit('new-message', { text, replyingTo })
    setReplyingTo(null)
  }

  return (
    <div className="h-full grid grid-rows-max-c-auto-max-c relative">
      {topBar}
      <ChatMessages
        messages={messages}
        onGetPrevious={getPrevious}
        ref={messagesRef}
      >
        {(message, i) => {
          if (user.ignored_users?.includes(message.user_id)) {
            return null
          } 
          return (
            <Message
              currentUsername={user.username}
              actionsBox={
                !hideActionsBox && (
                  <ActionsBox
                    message={message}
                    onReply={() => handleReplayClicked(i)}
                    onCensorMessage={censorMessages ? () => handleCensorMessage(i) : null}
                    onCreateReport={createReports ? () => handleCreateReport(i) : null}
                    onBanUser={banUser ? () => handleBanUser(message) : null}
                    onIgnoreUser={() => handleIgnoreUser(message)}
                    isDifferentUser={message.user_id != user.user_id}
                  />
                )
              }
              {...message}
            />
          )
        }}
      </ChatMessages>
      <ChatInput
        roomUsers={roomUsers}
        replyingTo={replyingTo}
        onCancelReplay={() => setReplyingTo(null)}
        onSend={sendMessage}
        disabled={isIgnoring}
        extra={isIgnoring ? (
          <IgnoringUserToast userId={receiverId} />
        ) : null}
      />
    </div>
  )
}