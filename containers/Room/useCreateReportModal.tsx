import { useRouter } from 'next/router'
import { useRef } from 'react'
import useConfirmModal from '../../components/ConfirmModal'
import { useModal } from '../../components/Modal'
import socket from '../../lib/socket'

export default function useCreateReportModal(messages) {

  const router = useRouter()
  const { openConfirm, closeConfirm } = useConfirmModal()
  const { modal, closeModal } = useModal()
  const newReportNotesInput = useRef<HTMLTextAreaElement>(null)

  const newReportForm = (
    <div>
      <div className="font-semibold mb-1">
        Notes
      </div>
      <textarea
        className="form-control w-full"
        placeholder="Why are you creating this report?"
        ref={newReportNotesInput}
      />
    </div>
  )

  const createReport = messageId => {
    const notes = newReportNotesInput.current.value
    socket.emit('admin-create-report', messageId, notes, r => {
      closeConfirm()
      const title = r.error ? r.errorMessage : `Report created!`
      const desc = r.error ? '' : (
        <div className="mt-4 text-center">
          <button className="btn btn-primary" onClick={() => {
            router.push(`/admin/reports/${r.data}?b=0`)
            closeModal()
          }}>
            View report
          </button>
        </div>
      )
      modal({ title, desc })
    })
  }

  const handleCreateReport = messageIndex => {
    const message = messages[messageIndex]
    const title = `Create report report for ${message.username}`
    const onConfirm = () => createReport(message.id)
    openConfirm({title, desc: newReportForm, confirmText: 'Create report', onConfirm, waitAfterConfirm: true })
  }

  return handleCreateReport
}