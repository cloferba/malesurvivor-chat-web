import UserItem from '../../components/UserItem'
import { User } from '../../lib/types'
import { useRecoilValue } from 'recoil'
import userState from '../../atoms/user'
import socket from '../../lib/socket'
import { useRouter } from 'next/router'
import { useMemo, useState } from 'react'

interface UsersListProps {
  users: Record<User['user_id'], User>,
  onMdClicked?: () => void,
  lowOpacityWhenOffline?: boolean,
}

export default function UsersList({
  users,
  onMdClicked,
  lowOpacityWhenOffline,
}: UsersListProps) {

  const [filterString, setFilterString] = useState('')
  const recoilUser = useRecoilValue<User>(userState)
  const router = useRouter()

  const sortedUsers: Partial<User[]> = Object.values(users).sort(({status}) => status == 'offline' ? 1 : -1)

  const filteredUsers = useMemo(() => {
    return sortedUsers.filter(({ username }) => {
      return username.includes(filterString)
    })
  }, [filterString, sortedUsers])

  const sendMd = user_id => {
    socket.emit('send-md', user_id, r => {
      if(r.data) {
        router.push(`/rooms/${r.data}`)
      }
      onMdClicked && onMdClicked()
    })
  }

  const firstOfflineIndex = filteredUsers.findIndex(({status}) => status == 'offline')

  const offlineDivider = (
    <div className="opacity-60 mt-3 mb-1 px-4">
      OFFLINE - {filteredUsers.length - firstOfflineIndex}
    </div>
  )

  return (
    <>
      <div className="mb-3 px-3">
        <input
          className="form-control bg-transparent w-full"
          placeholder="Filter users"
          onChange={e => setFilterString(e.target.value)}
          value={filterString}
        />
      </div>
      {filteredUsers.map((user, i) => {
        const showMdButton = recoilUser.user_id != user.user_id
        return (
          <div key={user.user_id}>
            {firstOfflineIndex == i && offlineDivider}
            <UserItem
              {...user}
              lowOpacityWhenOffline={lowOpacityWhenOffline}
              onSendMd={showMdButton ? () => sendMd(user.user_id) : null}
            />
          </div>
        )
      })}
    </>
  )
}