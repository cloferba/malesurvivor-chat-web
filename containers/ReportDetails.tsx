import { useRouter } from 'next/router'
import AdminLayout from '../containers/AdminLayout'
import useFetchData from '../hooks/useFetchData'
import ReportDetailTabs from '../components/ReportDetailTabs'
import { Report } from '../lib/types'
import AttendReportButton from '../containers/AttendReportButton'
import socket from '../lib/socket'

interface Data {
  data: Report,
  unauthorized: boolean,
  setData: (currentReport) => void
}

interface ReportDetails {
  children: (report: Report) => void
}

export default function ReportDetails({children}: ReportDetails) {

  const router = useRouter()
  const { reportId, b } = router.query
  const { data: report, unauthorized, setData }: Data = useFetchData('get-report', null, reportId)
  const [, , , , reportPage] = router.route.split('/')
  const path = reportPage || 'detail'

  return (
    <AdminLayout
      back={`/admin/reports?page=${b}`}
      title="Report details"
      unauthorized={unauthorized}
      extra={report && (
        <AttendReportButton
          setReport={setData}
          reportId={report.id}
          attended={report.attended}
        />
      )}
    >
      <div className="max-w-xl">
        <ReportDetailTabs
          reportId={reportId}
          page={b}
          path={path}
        />
        {report && children(report)}
      </div>
    </AdminLayout>
  )
}
