import Link from 'next/link'
import { MdPublic } from 'react-icons/md'
import useFetchData from '../hooks/useFetchData'

export default function PublicRoomsList({ onEnterRoomClicked = () => {} }) {

  const { data: rooms } = useFetchData('get-public-rooms', [])
  const { data: userRooms } = useFetchData('get-user-rooms', {})

  return (
    <>
      {rooms.map(room => (
        <div
          key={room.id}
          className="py-3 px-3 flex items-center hover:bg-gray-50 dark:hover:bg-gray-800 text-lg justify-between rounded-lg"
        >
          <div className="flex items-center font-medium">
            <div className="mr-2 opacity-40">
              <MdPublic/>
            </div>
            {room.name}
            <span className="opacity-70 ml-4">
              ({room.onlineUsersCount})
            </span>
          </div>
          <Link
            key={room.id}
            href={`/rooms/${room.id}`}
          >
            <a onClick={onEnterRoomClicked}>
              <button className={`btn-small  ${userRooms[room.id] ? 'border' : 'btn-primary'}`}>
                {userRooms[room.id] ? 'Enter' : 'Join'}
              </button>
            </a>
          </Link>
        </div>
      ))}
    </>
  )
}