import useConfirmModal from '../components/ConfirmModal'
import socket from '../lib/socket'

export default function AttendReportButton({ setReport, reportId, attended }) {

  const { openConfirm, closeConfirm } = useConfirmModal()

  function markAsAttended() {
    socket.emit('mark-report-as-attended', reportId, ({data}) => {
      setReport(report => ({
        ...report,
        ...data
      }))
      closeConfirm()
    })
  }

  function markAsAttendedClick() {
    openConfirm({
      title: 'Mark this report as attended?',
      confirmText: 'Accept',
      onConfirm: markAsAttended,
      waitAfterConfirm: true
    })
  }
  
  return (
    attended ? (
      <div className="px-1.5 text-white rounded bg-green-600">
        ATTENDED
      </div>
    ) : (
      <button
        className="btn-small btn-primary"
        onClick={markAsAttendedClick}
      >
        Mark as attended
      </button>
    )
  )
}