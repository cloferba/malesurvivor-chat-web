import Link from 'next/link'
import { ADMIN_PAGES } from '../lib/constants'
import SidebarItem from '../components/SidebarItem'
import { useRouter } from 'next/router'

export default function AdminTabs() {

  const { pathname } = useRouter()
  const [, , adminPage] = pathname.split('/')

  return (
    <>
      {ADMIN_PAGES.map(pageInfo => (
        <Link
          href={`/admin/${pageInfo.path}`}
          key={pageInfo.path}
        >
          <a>
            <SidebarItem
              icon={<pageInfo.icon />}
              label={pageInfo.label}
              active={adminPage == pageInfo.path}  
            />
          </a>
        </Link>
      ))}
    </>
  )
}
