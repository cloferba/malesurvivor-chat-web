import { useState } from 'react'
import { MdAdd, MdRemove } from 'react-icons/md'

export default function UserRoomsTitle({
  title,
  children,
  hasHiddenRooms = false,
}) {

  const [showHidden, setShowHidden] = useState(false)

  const toggleButtonLabel = showHidden
    ? <MdRemove/>
    : <MdAdd/>

  const toggleButton = (hasHiddenRooms) && (
    <span
      className="font-thin bg-black bg-opacity-10 p-1.5 rounded ml-auto cursor-pointer select-none"
      onClick={() => setShowHidden(!showHidden)}
    >
      {toggleButtonLabel}
    </span>
  )

  return (
    <>
      <div className="px-3 py-2 flex items-center">
        <h4>{title}</h4>
        {toggleButton}
      </div>
      {children(showHidden)}
    </>
  )
}