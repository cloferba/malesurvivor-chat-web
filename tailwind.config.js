module.exports = {
  purge: [
    "./pages/**/*.{js,jsx,ts,tsx}",
    "./components/**/*.{js,jsx,ts,tsx}",
    "./containers/**/*.{js,jsx,ts,tsx}",
    "./hooks/**/*.{js,jsx,ts,tsx}",
    "./lib/utils.ts",
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'sm': '350px'
      },
      gridTemplateRows: {
        'max-c-auto-max-c': 'max-content auto max-content',
        'max-c-auto': 'max-content auto'
      },
      colors: {
        accent: 'var(--color-accent)',
        'forum-blue': 'var(--color-forum-blue)',
        'forum-dark-blue': 'var(--color-forum-dark-blue)',
      },
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      cursor: ['disabled'],
      pointerEvents: ['disabled'],
      display: ['group-hover'],
      height: ['group-hover'],
      backgroundColor: ['even']
    }
  },
  plugins: [],
}
