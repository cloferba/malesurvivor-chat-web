export interface UserPermissions {
  addAndRemoveKeywords: boolean,
  adminPanelAccess: boolean,
  createRooms: boolean,
  handleReports: boolean,
  useOfflineStatus: boolean,
  censorMessages: boolean,
  createReports: boolean,
  banUser: boolean,
  getBannedUsers: boolean,
}

export interface User {
  email: string,
  timezone: string,
  user_group_id: string,
  user_id: string,
  username: string,
  status: string,
  avatar_url_m: string,
  permissions: UserPermissions,
  ban: {
    type: string,
    until: number
  },
  ignored_users: string[],
}

export interface ReplyMessage {
  username: string,
  text: string
}

export interface Message {
  id: string,
  text: string,
  createdAt: Date,
  censored: boolean,
  username: string,
  avatar_url_m: string,
  user_id: string,
  replyingTo: ReplyMessage,
}

export interface Room {
  id: string,
  name: string,
  createdAt: Date,
  users: string[],
  type: string,
  disabled: boolean,
  hasNotification: boolean,
  onlineUsersCount: number,
}

export interface Report {
  id: string,
  createdAt: Date,
  messageId: string,
  roomId: string,
  type: string,
  message: Message,
  room: Room,
  attended: boolean,
  attendedAt: Object,
  attendedById: string,
  attendedBy: User,
  resolution: string
}

export interface Keyword {
  id: string,
  incidences: number,
  deleted: boolean,
  text: string,
  createdAt: Date
}

export interface Notification {
  id: string,
  type: string,
  title: string,
  desc: string,
  reportId: string,
  url: string,
  createdAt: Date,
  icon: any
}