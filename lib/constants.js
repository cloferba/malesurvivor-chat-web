import {
  MdCheckCircle,
  MdAccessTime,
  MdDoNotDisturbOn,
  MdRadioButtonUnchecked,
  MdForum,
  MdFlag,
  MdErrorOutline,
  MdAnnouncement,
  MdVisibility,
  MdPersonOutline,
  MdCancel
} from 'react-icons/md'

export const typesOfBan = {
  GENERAL_BAN: 'generalBan',
  PRIVATE_MESSAGES_BAN: 'privateMessagesBan'
}

export const readableTypesOfBan = [
  {
    label: 'General ban',
    value: 'generalBan' 
  },
  {
    label: 'Private messages ban',
    value: 'privateMessagesBan' 
  }
]

export const typesOfBanParsed = {
  generalBan: 'General ban',
  privateMessagesBan: 'Private messages ban'
}

export const APP_NAME = 'Male Survivor Chat'
export const FORUM_NAME = 'MaleSurvivor Forum'
export const CHAT_URL = 'https://chat.malesurvivor.org/'
export const FORUM_URL = 'https://forum.malesurvivor.org/'
export const FORUM_LOGIN_URL = 'http://forum.malesurvivor.org/index.php?login/login'

export const STATUS_TYPES = {
  online: {
    icon: MdCheckCircle,
    iconColor: '#1976d2',
    id: 'online',
    label: 'Online'
  },
  away: {
    icon: MdAccessTime,
    iconColor: '#ffb300',
    id: 'away',
    label: 'Away'
  },
  doNotDisturb: {
    icon: MdDoNotDisturbOn,
    iconColor: '#f4511e',
    id: 'doNotDisturb',
    label: "Don't disturb",
    desc: 'You will not receive notifications'
  },
  offline: {
    icon: MdRadioButtonUnchecked,
    iconColor: '#90a4ae',
    id: 'offline',
    label: 'Offline',
  }
}

export const ADMIN_PAGES = [
  {
    icon: MdForum,
    label: 'Rooms',
    path: 'rooms',
  },
  {
    icon: MdFlag,
    label: 'Reports',
    path: 'reports'
  },
  {
    icon: MdErrorOutline,
    label: 'Keywords',
    path: 'keywords'
  },
  {
    icon: MdCancel,
    label: 'Banned users',
    path: 'banned-users'
  }
]

export const REPORT_TYPES = {
  sensitiveInformation: {
    label: 'Sensitive information',
    icon: MdAnnouncement
  },
  keyword: {
    label: 'Contains a keyword',
    icon: MdVisibility
  },
  createdByModerator: {
    label: 'Created by user',
    icon: MdPersonOutline,
    additionalInfo: [
      {
        name: 'notes',
        label: 'Notes',
      }, {
        name: 'createdBy',
        label: 'Created by'
      }
    ]
  }
}

export const USER_GROUPS = [
  { label: 'Administrative (default)', value: 3, disabled: true },
  { label: 'Annual Members', value: 27 },
  { label: 'At-Risk', value: 31 },
  { label: 'Family and Friends', value: 9 },
  { label: 'Greeters', value: 19 },
  { label: 'Moderators', value: 4 },
  { label: 'Registered', value: 2 },
  { label: 'Sustaining Members', value: 33 },
  { label: 'WoR Alumnus', value: 32 },
]