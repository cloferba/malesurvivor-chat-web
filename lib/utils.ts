export const getOnlyDate = (date: Date) => {
  const d = new Date(date)
  return d.toLocaleDateString('en-US')
}

export const getDateTime = (date: Date) => {
  const d = new Date(date)
  return d.toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true })
}

export const getDateAndTime = (date: Date) => {
  return `${getOnlyDate(date)} - ${getDateTime(date)}`
}

// export function getMinutesAgo(date) {
//   return dateDiffInDays(date, new Date())
// }

const _MS_PER_DAY = 1000 * 60
export function dateDiffInMinutes(a, b) {
  const utc1 = Date.parse(a)
  const utc2 = Date.parse(b)
  return Math.floor((utc2 - utc1) / _MS_PER_DAY)
}

export const isToday = (date: Date) => {
  const today = new Date()
  const d = new Date(date)
  return d.getDate() == today.getDate() &&
    d.getMonth() == today.getMonth() &&
    d.getFullYear() == today.getFullYear()
}

export function sameDay(d1Str: Date, d2Str: Date) {
  const d1 = new Date(d1Str)
  const d2 = new Date(d2Str)
  return d1.getDate() === d2.getDate()  &&
    d1.getMonth() === d2.getMonth() &&
    d1.getFullYear() === d2.getFullYear()
}