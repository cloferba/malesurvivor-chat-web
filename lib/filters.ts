import { REPORT_TYPES, typesOfBanParsed } from './constants'
import { sameDay } from './utils'

export const defaultOption = {
  label: 'All',
  value: 'all'
}

export const bannedUsersFilters = [
  {
    label: 'Username',
    type: 'text',
    name: 'username',
    defaultValue: defaultOption.value,
    filterHandler: ({username}, filterValue) => (
      username.toLowerCase().includes(filterValue.toLowerCase())
    ),
  },
  {
    label: 'Type of ban',
    type: 'select',
    options: [
      defaultOption,
      { label: 'General ban', value: 'General ban' },
      { label: 'Private messages ban', value: 'Private messages ban' }
    ],
    name: 'type',
    defaultValue: defaultOption.value,
    filterHandler: ({ ban }, filterValue: string) => {
      if (filterValue === defaultOption.value) return true
      return typesOfBanParsed[ban.type] === filterValue
    },
  },
]

export const reportsFilters = [
  {
    label: 'Reason',
    type: 'select',
    options: [ defaultOption, ...Object.keys(REPORT_TYPES).map(value => ({ 
      label: REPORT_TYPES[value].label,
      value
    }))],
    name: 'type',
    defaultValue: defaultOption.value,
    filterHandler: ({type}, filterValue) => type == filterValue,
  },
  {
    label: 'Attended',
    type: 'select',
    options: [
      defaultOption,
      { label: 'Attended', value: 'true' },
      { label: 'Not attended', value: 'false' }
    ],
    name: 'attended',
    defaultValue: defaultOption.value,
    filterHandler: ({attended}, filterValue) => filterValue == 'true' ? attended : !attended,
  },
  {
    label: 'Username',
    type: 'text',
    name: 'username',
    filterHandler: ({message}, filterValue) => {
      const username = message.username.toLowerCase()
      return username.includes(filterValue.toLowerCase())
    },
  },
  {
    label: 'Room',
    type: 'text',
    name: 'room_name',
    filterHandler: ({room}, filterValue) => {
      const roomName = room.name?.toLowerCase() || ''
      return roomName.includes(filterValue.toLowerCase())
    },
  },
  {
    label: 'Message',
    type: 'text',
    name: 'message',
    filterHandler: ({message}, filterValue) => {
      const messageText = message.text.toLowerCase()
      return messageText.includes(filterValue.toLowerCase())
    },
  },
  {
    label: 'From',
    type: 'date',
    name: 'from',
    filterHandler: ({createdAt}, filterValue) => {
      const reportDate = new Date(createdAt)
      const filterDate = new Date(filterValue)
      if(sameDay(reportDate, filterDate)) return true
      return reportDate > filterDate
    },
  },
  {
    label: 'to',
    type: 'date',
    name: 'to',
    filterHandler: ({createdAt}, filterValue) => {
      const reportDate = new Date(createdAt)
      const filterDate = new Date(filterValue)
      if(sameDay(reportDate, filterDate)) return true
      return reportDate < filterDate
    },
  }
]