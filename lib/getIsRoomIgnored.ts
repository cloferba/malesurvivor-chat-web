import { User } from './types'

export default function getIsRoomIgnored(user: User, users: User['user_id'][]) {
  if (!users) return { isIgnoring: false }
  const receiverId = users.find(userId => userId != user.user_id)
  if (user.ignored_users?.includes(receiverId)) {
    return { isIgnoring: true, receiverId } 
  } else {
    return { isIgnoring: false }
  }
}