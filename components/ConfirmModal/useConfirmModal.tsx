import ConfirmButtons from './ConfirmButtons'
import { useModal } from '../Modal'

interface OpenConfirmProps {
  title: any,
  desc?: any,
  confirmText?: string,
  onConfirm?: any,
  waitAfterConfirm?: boolean
}

export default function useConfirmModal() {

  const { modal, closeModal } = useModal()

  const openConfirm = ({
    title,
    desc,
    confirmText = 'Confirm',
    onConfirm,
    waitAfterConfirm = false
  }: OpenConfirmProps) => {
    const modalDesc = (
      <div>
        <div className="mt-3 mb-5">
          {desc}
        </div>
        <ConfirmButtons
          confirmText={confirmText}
          onCancel={closeModal}
          onConfirm={onConfirm}
          waitAfterConfirm={waitAfterConfirm}
        />
      </div>
    )

    modal({
      title,
      desc: modalDesc,
      hideDefaultButton: true
    })
  }


  return {
    openConfirm,
    closeConfirm: closeModal
  }
  
}