import { useState } from 'react'
import { AiOutlineLoading } from 'react-icons/ai'

export default function ConfirmButtons({
  cancelText = 'Cancel',
  confirmText = 'Confirm',
  onCancel = () => {},
  onConfirm = () => {},
  waitAfterConfirm = false
}) {

  const [waiting, setWaiting] = useState(false)

  const confirmClicked = () => {
    waitAfterConfirm && setWaiting(true)
    onConfirm()
  }

  const waitingIcon = waiting ? (
    <div className="animate-spin h-4 w-4 mr-2">
      <AiOutlineLoading />
    </div>
  ) : null

  return (
    <div className="flex items-center space-x-2 justify-end">
      <button
        className="btn border"
        onClick={onCancel}
      >
        {cancelText}
      </button>
      <button
        className="btn btn-primary"
        onClick={confirmClicked}
        disabled={waiting}
      >
        <div className="flex items-center">
          {waitingIcon}
          {confirmText}
        </div>
      </button>
    </div>
  )
}