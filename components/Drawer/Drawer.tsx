import { motion, AnimatePresence } from 'framer-motion'

const backdropVariants = {
  hidden: { opacity: 0, },
  visible: { opacity: 0.5, }
}

export default function Drawer({
  visible,
  onClose = () => {},
  children,
  placement = 'left'
}) {

  const initialX = placement == 'left' ? '-100%' : '100%'

  const drawerVariants = {
    hidden: { x: initialX, },
    visible: { x: '0%', }
  }
  
  const backdrop = (
    <motion.div
      initial="hidden"
      animate="visible"
      exit="hidden"
      variants={backdropVariants}
      onClick={onClose}
      className="top-0 left-0 absolute w-full h-full bg-black"
    />
  )

  function handlePanEnd(e, info) {
    const { velocity: { x }} = info
    if(placement == 'left' && x < -5) {
      onClose()
    }
    if(placement == 'right' && x > 5) {
      onClose()
    }
  }

  return(
    <AnimatePresence>
      {visible && (
        <motion.div
          className={`fixed flex ${placement == 'right' ? 'justify-end' : ''} top-0 left-0 w-full z-40 h-screen touch-action-none`}
          onPanEnd={handlePanEnd}
        >
          <motion.div
            initial="hidden"
            animate="visible"
            exit="hidden"
            variants={drawerVariants}
            transition={{type: 'tween', ease: 'easeInOut'}}
            className="relative h-full w-full xs:w-10/12 sm:w-64 bg-white z-10"
          >
            {children}
          </motion.div>
          {backdrop}
        </motion.div>
      )}
    </AnimatePresence>
  )
}