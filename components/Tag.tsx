export default function Tag({ text, className = 'bg-gray-600 text-white' }) {
  return (
    <div className={`${className} rounded px-1.5 mr-3 text-sm`}>
      {text}
    </div>
  )
}