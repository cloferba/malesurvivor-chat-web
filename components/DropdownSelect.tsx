import { useState } from 'react'
import { MdExpandMore } from 'react-icons/md'
import Dropdown from './Dropdown/Dropdown'

export default function DropdownSelect({options, onChange, value = null}) {
  
  const [open, setOpen] = useState(false)

  const selectedOption = options.find(({value: optionValue}) => optionValue == value)

  function optionClicked(newValue) {
    onChange && onChange(newValue)
    setOpen(false)
  }

  const menu = (
    <div className="w-52 shadow-lg bg-gray-50 dark:bg-gray-700 rounded py-2 mt-1 border dark:border-gray-500">
      {options.map(({label, value}) => (
        <div
          key={value}
          className="py-2 px-3 cursor-pointer hover:bg-gray-100 dark:hover:bg-gray-600"
          onClick={() => optionClicked(value)}
        >
          {label}
        </div>
      ))}
    </div>
  )

  return (
    <div className="flex items-center rounded" >
      <Dropdown
        overlay={menu}
        open={open}
        onOpenChange={setOpen}
      >
        <div className="flex items-center rounded bg-gray-200 dark:bg-gray-700 py-1 pl-3 pr-1 cursor-pointer hover:bg-gray-300 dark:hover:bg-gray-600">
          {selectedOption ? selectedOption.label : ''}
          <MdExpandMore size={20} className="ml-1" />
        </div>
      </Dropdown>
    </div>
  )
}