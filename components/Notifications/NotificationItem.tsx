import Link from 'next/link'
import { MdFlag, MdForum, MdChat } from 'react-icons/md'
import clsx from 'clsx'

const IconsByType = {
  'discussion-message': MdForum,
  'new-report': MdFlag,
  'md-message': MdChat
}

export default function NotificationItem({ isNew = false, url, title, desc, type }) {

  const Icon = IconsByType[type]

  const dot = isNew && (
    <div className="w-3 h-3 rounded-full bg-blue-400 absolute top-2 right-3" />
  )

  const itemClasses = clsx(
    'flex relative px-3 py-2 cursor-pointer',
    isNew ? 'bg-yellow-50 hover:bg-yellow-100 dark:bg-purple-800 dark:hover:bg-purple-700' : 'hover:bg-gray-100 dark:hover:bg-gray-700'
  )

  return (
    <Link href={url}>
      <a className={itemClasses} >
        <div className="flex-shrink-0 mr-3 mt-1">
          <Icon size={22}/>
        </div>
        <div>
          <div>
            {title}
          </div>
          <div className="text-sm opacity-50">
            {desc}
          </div>
        </div>
        {dot}
      </a>
    </Link>
  )
}