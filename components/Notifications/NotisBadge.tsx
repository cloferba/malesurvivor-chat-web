import { MdNotificationsNone, MdNotificationsActive } from 'react-icons/md'
import IconButton from '../IconButton'

function formatNumber(number) {
  if(number < 100) return number
  return '99+'
}

export default function NotisBadge({darkMode, newNotifications}) {

  const thereAreNewNotifications = Boolean(newNotifications)

  const dot = thereAreNewNotifications && (
    <div className="px-1 bg-gray-700 text-white rounded absolute bottom-1.5 left-6 flex items-center justify-center text-xs ml-0.5">
      {formatNumber(newNotifications)}
    </div>
  )

  return (
    <div className="relative">
      <IconButton
        className={thereAreNewNotifications ? 'bg-accent text-white hover:bg-accent' : 'opacity-70'}
        icon={thereAreNewNotifications ? MdNotificationsActive : MdNotificationsNone}
        onClick={() => {}}
        darkMode={darkMode}
      />
      {dot}
    </div>
  )
}