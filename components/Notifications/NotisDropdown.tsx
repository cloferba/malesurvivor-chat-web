import { useEffect, useState } from 'react'
import useSound from 'use-sound'
import { Notification } from '../../lib/types'
import socket from '../../lib/socket'
import Dropdown from '../Dropdown'
import NotisBadge from './NotisBadge'
import NotisList from './NotisList'
import useFetchData from '../../hooks/useFetchData'

export default function NotisDropdown({darkMode}) {

  const { data, setData } = useFetchData('get-last-notifications', [])
  const [newNotifications, setNewNotifications] = useState(0)
  const [lastNewNotifications, setLastNewNotifications] = useState(0)
  const [playNotificationSound] = useSound('/noti-sound.mp3')
  const { notifications = [], newNotifications: updatedNewNotifications = 0 } = data

  useEffect(() => {
    setNewNotifications(updatedNewNotifications)
    setLastNewNotifications(updatedNewNotifications)
  }, [updatedNewNotifications, setNewNotifications, setLastNewNotifications])

  useEffect(() => {
    socket.on('notifications/add', (notification: Notification) => {
      setData(currentState => ({
        ...currentState,
        notifications: [
          notification,
          ...currentState.notifications
        ]
      }))
      playNotificationSound()
      setNewNotifications(currentNotifications => currentNotifications + 1)
      setLastNewNotifications(currentLastNotifications => currentLastNotifications + 1)
    })
    return () => socket.off('notifications/add')
  }, [setData, setNewNotifications, setLastNewNotifications, playNotificationSound])

  const handleOpenChange = open => {
    open && setNewNotifications(0)
    socket.emit('notifications-opened')
  }

  const notisList = (
    <NotisList
      notifications={notifications}
      newNotifications={lastNewNotifications}
    />
  )
  
  return (
    <div>
      <Dropdown
        overlay={notisList}
        placement="right"
        onOpenChange={handleOpenChange}
      >
        <NotisBadge
          darkMode={darkMode}
          newNotifications={newNotifications}
        />
      </Dropdown>
    </div>
  )
}