import Link from 'next/link'
import { Notification } from '../../lib/types'
import NotificationItem from './NotificationItem'

interface NotificationsListProps {
  notifications: Notification[],
  newNotifications: number,
}

export default function NotificationsList({notifications, newNotifications = 0}: NotificationsListProps) {

  const allNotificationsButton = (
    <div className="px-2 mt-3">
      <Link href="/notifications">
        <a>
          <button className="btn-small border w-full">
            All notifications
          </button>
        </a>
      </Link>
    </div>
  )

  const notisList = (
    <>
      <div className="max-h-60 overflow-y-auto">
        {notifications.map((notification, i) => (
          <NotificationItem
            key={notification.id}
            isNew={i < newNotifications}
            {...notification}
          />
        ))}
      </div>
      {allNotificationsButton}
    </>
  )

  const emptyList = (
    <div className="text-center opacity-60 py-3">
      There aren't notifications yet
    </div>
  )

  return (
    <div className="bg-gray-50 dark:bg-gray-800 rounded w-72 py-2 shadow-lg mt-1 border border-gray-500">
      {notifications.length ? notisList : emptyList}
    </div>
  )
}