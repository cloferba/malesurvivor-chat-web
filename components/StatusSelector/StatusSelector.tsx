import socket from '../../lib/socket'
import { useRecoilValue } from 'recoil'
import userState from '../../atoms/user'
import { STATUS_TYPES } from '../../lib/constants'
import Dropdown from '../Dropdown'
import { useState } from 'react'
import clsx from 'clsx'
import { MdLogout } from 'react-icons/md'

const defaultTheme = process.env.NEXT_PUBLIC_DEFAULT_THEME

const selectClassName = clsx(
  'rounded w-44 py-2 shadow-lg mt-2',
  defaultTheme ? 'bg-gray-50' : 'bg-gray-800'
)

const bgOptionActive = defaultTheme ? 'bg-gray-100' : 'bg-gray-700'
const bgOptionHover = defaultTheme ? 'hover:bg-gray-100' : 'hover:bg-gray-700'

const MenuItem = ({
  active,
  onClick,
  icon: Icon,
  iconColor = '#90a4ae',
  label,
  desc
}: any) => (
  <div
    onClick={onClick}
    className={`px-3 py-2 cursor-pointer ${bgOptionHover} ${active ? bgOptionActive : ''}`}
  >
    <div className="flex items-center">
      <div className="mr-2">
        <Icon color={iconColor} />
      </div>
      <div className="font-medium">
        {label}
      </div>
    </div>
    
    {desc && (
      <div className="text-sm opacity-60 pl-6 mt-1">
        {desc}
      </div>
    )}
  </div>
)

const Select = ({currentStatus, onChange, showOffline = false, onLogout }) => {

  const types = Object.values(STATUS_TYPES).filter(({id}) => id == 'offline' ? showOffline : true)

  return (
    <div className={selectClassName} >
      {types.map(statusType => (
        <MenuItem
          key={statusType.id}
          active={currentStatus == statusType.id}
          onClick={() => onChange(statusType.id)}
          {...statusType}
        />
      ))}
      {/* <hr className="my-3 opacity-30" />
      <MenuItem
        onClick={onLogout}
        icon={MdLogout}
        label="Logout"
      /> */}
    </div>
  )
}

const triggerClassName = clsx(
  'px-2 py-1 rounded cursor-pointer flex items-center',
  defaultTheme
    ? 'bg-gray-50 hover:bg-white'
    : 'bg-gray-700 bg-opacity-30 hover:bg-opacity-40'
)

export default function StatusSelector({ showOffline }) {

  const user = useRecoilValue(userState)
  const [open, setOpen] = useState(false)

  const setStatus = status => {
    if(status != user.status) {
      socket.emit('update-status', status)
    }
    setOpen(false)
  }

  const logout = () => {
    document.cookie = 'xf_session=;expires=Thu, 01 Jan 1970 00:00:01 GMT;'
    document.cookie = 'xf_user=;expires=Thu, 01 Jan 1970 00:00:01 GMT;'
    document.location.reload()
  }

  const options = (
    <Select
      currentStatus={user.status}
      onChange={setStatus}
      onLogout={logout}
      showOffline={showOffline}
    />
  )

  const status = STATUS_TYPES[user.status]

  return (
    <Dropdown
      overlay={options}
      onOpenChange={setOpen}
      open={open}
    >
      <div className={triggerClassName}>
        <status.icon
          color={status.iconColor}
          className="bg-white rounded-full mr-2"
        />
        {status.label}
      </div>
    </Dropdown>
  )
}