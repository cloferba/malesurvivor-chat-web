import { useSetRecoilState } from 'recoil'
import userState from '../../atoms/user'
import useFetchData from '../../hooks/useFetchData'
import socket from '../../lib/socket'
import { User } from '../../lib/types'
import Avatar from '../Avatar'
import List from '../List'

export default function IgnoredUsersModal() {

  const setUser = useSetRecoilState(userState)
  const { data, loading, setData } = useFetchData('get-ignored-users', [])
  
  const unIgnore = userId => {
    socket.emit('unignore-user', userId, ignored_users => {
      const index = data.findIndex(user => user.user_id == userId)
      const newData = [...data]
      newData.splice(index, 1)
      setData(newData)
      setUser(currUser => ({ ...currUser, ignored_users }))
    })
  }

  if (loading) {
    return (
      <div className="opacity-30 select-none">
        Loading...
      </div>
    )
  }

  return (
    <div className="mt-5 space-y-4">
      <List data={data}>
        {(user: User) => (
          <div
            key={user.user_id}
            className="flex items-center"
          >
            <Avatar
              src={user.avatar_url_m}
              size="small"
            />
            <h5 className="ml-3">
              {user.username}
            </h5>
            <button
              className="ml-auto btn border"
              onClick={() => unIgnore(user.user_id)}
            >
              Stop ignoring
            </button>
          </div>
        )}
      </List>
    </div>
  )
}