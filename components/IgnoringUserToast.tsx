import { useSetRecoilState } from 'recoil'
import userState from '../atoms/user'
import socket from '../lib/socket'

export default function IgnoringUserToast({ userId }) {

  const setUser = useSetRecoilState(userState)

  const unIgnore = () => {
    socket.emit('unignore-user', userId, ignored_users => {
      setUser(currUser => ({ ...currUser, ignored_users }))
    })
  }

  return (
    <div className="bg-gray-200 dark:bg-gray-700 py-2.5 px-3 rounded-md mb-5 text-center md:flex items-center justify-between">
      <span>You are ignoring this user and cannot send messages here.</span>
      <div className="mt-3 md:mt-0">
        <button
          className="btn-small border"
          onClick={unIgnore}
        >
          Stop ignoring
        </button>
      </div>
    </div>
  )
}