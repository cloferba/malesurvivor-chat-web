import { useRouter } from 'next/router'

export default function useFilters(filters, array) {
  const { query } = useRouter()

  if(!query) return array

  let applyingFilters = false

  for(const queryKey in query) {
    const filter = filters.find(({name}) => queryKey == name)
    if(!filter) continue
    applyingFilters = true
    array = array.filter(report => filter.filterHandler(report, query[queryKey]))
  }

  return { array, applyingFilters }
}