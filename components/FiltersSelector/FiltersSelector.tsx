import { useEffect, useState } from 'react'
import DropdownSelect from '../DropdownSelect'
import { useRouter } from 'next/router'
import { defaultOption } from '../../lib/filters'

function InputDate({ value, onChange, name, selectedFilters }) {

  const limitProp = name == 'from' ? 'to' : 'from'
  const inputProp = limitProp == 'to' ? 'max' : 'min'
  const inputLimit = { [inputProp]: selectedFilters[limitProp] }

  return (
    <div className="flex items-center space-x-3">
      <input
        onChange={e => onChange(e.target.value)}
        className="form-control bg-transparent flex-1"
        type="date"
        value={value}
        { ...inputLimit }
      />
      <button
        className="btn border"
        onClick={() => onChange('')}
        disabled={!value}
      >
        Clear
      </button>
    </div>
  )
}

function TextInput({ value, onChange}) {
  return (
    <input
      onChange={e => onChange(e.target.value)}
      className="form-control bg-transparent w-full"
      type="text"
      value={value}
    />
  )
}

function FilterItem({ type, ...props }) {
  const mapComponentTypes = {
    text: TextInput,
    date: InputDate,
    select: ({options, onChange, value}) => (
      <DropdownSelect
        options={options}
        onChange={onChange}
        value={value || defaultOption.value}
      />
    )
  }
  const Component = mapComponentTypes[type]
  return <Component {...props} />
}

export default function FiltersSelector({ fields, onAccept }) {

  const [selectedFilters, setSelectedFilters] = useState({})

  const router = useRouter()

  useEffect(() => {
    router.query && setSelectedFilters(router.query)
  }, [router.query])

  function handleItemChange(value, name) {
    const filter = fields.find(({name: filterName}) => filterName == name)
    const isNotDefault = value != filter.defaultValue
    setSelectedFilters(currentFilters => {
      if(value && isNotDefault) return { ...currentFilters, [name]: value}
      let newFilters = { ...currentFilters }
      delete newFilters[name]
      return newFilters
    })
  }

  function applyFilters() {
    router.push({ query: { ...selectedFilters, page: 0 } })
    onAccept()
  }

  function clearAll() {
    router.push({ query: { page: 0 } })
    onAccept()
  }

  return (
    <div>
      {fields.map(filter => (
        <div
          key={filter.name}
          className="my-4"
        >
          <div className="font-medium mb-1">
            {filter.label}
          </div>
          <FilterItem
            { ...filter }
            onChange={value => handleItemChange(value, filter.name)}
            selectedFilters={selectedFilters}
            value={selectedFilters[filter.name] || ''}
          />
        </div>
      ))}
      <div className="flex items-center justify-between mt-24">
        <button
          className="btn border"
          onClick={clearAll}
        >
          Clear all
        </button>
        <button
          className="btn btn-primary"
          onClick={applyFilters}
        >
          Apply filters
        </button>
      </div>
    </div>
  )
}