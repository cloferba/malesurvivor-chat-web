export default function IconButton({
  icon: Icon,
  onClick = () => {},
  darkMode = false,
  className = '',
  ...props
}) {
  return (
    <div
      className={`p-2 cursor-pointer hover:bg-gray-200 dark:hover:bg-opacity-10 ${darkMode ? 'hover:bg-opacity-10 text-white' : ''} inline-flex items-center justify-center rounded-full ${className}`}
      onClick={onClick}
      {...props}
    >
      <Icon size={26} />
    </div>
  )
}