import { MdSecurity } from 'react-icons/md'

export default function UnauthorizedToast() {
  return(
    <div className="flex border border-red-400 rounded space-x-3 items-center p-3">
      <MdSecurity/>
      <div className="ml-3">
        You have no permission to see this page
      </div>
    </div>
  )
}