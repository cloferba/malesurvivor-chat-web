import clsx from 'clsx'

const defaultTheme = process.env.NEXT_PUBLIC_DEFAULT_THEME

interface SidebarItemProps {
  icon?: any,
  label: any,
  active?: boolean,
  hasNotification?: boolean,
}

const dot = (
  <div className="w-2 h-2 rounded-full bg-blue-400 overflow-hidden ml-2" />
)

const defaultClass = clsx(
  'py-2 px-3 flex items-center cursor-pointer',
  defaultTheme ? 'hover:bg-gray-50' : 'hover:bg-white hover:bg-opacity-10'
)

const activeClass = clsx(
  defaultTheme ? 'bg-gray-50' : 'bg-white bg-opacity-20',
  defaultTheme ? 'border-r-4 border-blue-400 text-forum-blue dark:text-forum-dark-blue' : 'text-white'
)

export default function SidebarItem({ icon, label, active, hasNotification }: SidebarItemProps) {

  const itemClass = clsx(
    defaultClass,
    active ? activeClass : null,
    hasNotification ? 'font-semibold' : null
  )

  const iconClass = clsx(
    'mr-2',
    active ? defaultTheme ? 'text-blue-400' : null : 'opacity-40'
  )

  return (
    <div className={itemClass}>
      <div className={iconClass}>
        {icon}
      </div>
      {label}
      {hasNotification && dot}
    </div>
  )
}