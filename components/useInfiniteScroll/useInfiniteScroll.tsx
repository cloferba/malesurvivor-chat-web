import { useEffect } from 'react'

export default function useInfiniteScroll({
  onTapTop,
  onTapBottom,
  scrollTarget,
}) {

  const keepScrollPosition = (previousHeight) => {
    const newScrollTop = scrollTarget.scrollHeight - previousHeight
    scrollTarget.scroll({ top: newScrollTop })
  }

  const handleScroll = e => {
    const { scrollTop, clientHeight, scrollHeight } = e.target
    if(scrollTop == 0) {
      const previousHeight = scrollTarget.scrollHeight
      onTapTop(() => keepScrollPosition(previousHeight))
      return
    }
    const isAtTheBottom = scrollTop + clientHeight == scrollHeight
    isAtTheBottom && onTapBottom()
  }

  useEffect(() => {
    if(!scrollTarget) return
    scrollTarget.addEventListener('scroll', handleScroll)
    return () => scrollTarget.removeEventListener('scroll', handleScroll)
  }, [scrollTarget, handleScroll])

}