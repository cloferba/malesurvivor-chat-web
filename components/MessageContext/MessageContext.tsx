import { useEffect, useState, useRef } from 'react'
import useFetchData from '../../hooks/useFetchData'
import socket from '../../lib/socket'
import ChatMessages from '../ChatMessages'
import Message from '../Message'

export default function MessageContext({ roomId, messageId}) {

  const { data: messages, setData } = useFetchData('get-context-messages', [], roomId, messageId)
  const containerRef = useRef<HTMLElement>()
  const messageRef = useRef<HTMLElement>()
  const [firstTime, setFirstTime] = useState(true)

  useEffect(() => {
    if(firstTime && messageRef.current) {
      const container = containerRef.current
      container.scroll({top: messageRef.current.offsetTop - container.offsetHeight / 2 })
      setFirstTime(false)
    }
  }, [messages.length, messageRef.current])

  const handleGetPrevious = cb => {
    const fistMessageId = messages[0].id
    socket.emit('get-previous-messages', roomId, fistMessageId, previousMessages => {
      setData(currentMessages => [
        ...previousMessages,
        ...currentMessages,
      ])
      cb()
    })
  }

  const handleGetFollowing = cb => {
    const lastMessageId = messages[messages.length - 1].id
    socket.emit('get-following-messages', roomId, lastMessageId, followingMessages => {
      setData(currentMessages => [
        ...currentMessages,
        ...followingMessages
      ])
      cb()
    })
  }

  return (
    <div className="h-96 rounded-lg overflow-hidden border dark:border-gray-500 max-w-md relative">
      <ChatMessages
        messages={messages}
        onGetPrevious={handleGetPrevious}
        onGetFollowing={handleGetFollowing}
        ref={containerRef}
      >
        {message => (
          <Message
            highlighted={message.id == messageId}
            innerRef={message.id == messageId ? messageRef : null}
            {...message}
          />
        )}
      </ChatMessages>
    </div>
  )
}