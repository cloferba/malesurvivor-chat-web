import { ModalOptions } from './types'

const Modal = ({title, desc, hideDefaultButton, defaultButtonText = 'Accept', onAccept, height}: ModalOptions) => (
  <div
    className="w-11/12 z-20 max-w-md rounded-lg bg-white dark:bg-gray-800 dark:text-white p-5 relative max-h-full overflow-y-auto"
    style={height ? { height } : null}
  >
    <h3 className="text-center">
      {title}
    </h3>

    {desc}

    {!hideDefaultButton && (
      <div className="mt-8 text-right">
        <button
          onClick={onAccept}
          className="btn btn-primary"
        >
          {defaultButtonText}
        </button>
      </div>
    )}
  </div>
)
export default Modal