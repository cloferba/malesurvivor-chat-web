export interface ModalOptions {
  title?: string,
  desc?: any,
  hideDefaultButton?: boolean,
  defaultButtonText?: String,
  onAccept?: () => void,
  height?: number
}