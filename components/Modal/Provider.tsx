import ModalContext from './context'
import { useState } from 'react'
import Modal from './Modal'
import { ModalOptions } from './types'

export default function Provider({children}: {children: any}) {

  const [opened, setOpened] = useState(false)
  const [options, setOptions] = useState<ModalOptions>()

  const modal = (options: ModalOptions) => {
    setOptions(options)
    setOpened(true)
  }

  const closeModal = () => setOpened(false)

  const updateModal = updateObj => {
    setOptions(currentOptions => ({...currentOptions, ...updateObj}))
  }

  return (
    <ModalContext.Provider value={{modal, closeModal, updateModal}}>
      {children}
      {opened && (
        <div className="z-40 fixed w-screen h-screen left-0 top-0 flex justify-center items-center py-6">
          <Modal
            onAccept={closeModal}
            {...options}
          />
          <div
            className="opacity-50 bg-black w-full h-full absolute z-10"
            onClick={closeModal}
          />
        </div>
      )}
    </ModalContext.Provider>
  )
}