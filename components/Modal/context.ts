import { createContext } from 'react'
import { ModalOptions } from './types'

interface ModalContext {
  modal: (options: ModalOptions) => void,
  closeModal: () => void,
  updateModal: (options: Partial<ModalOptions>) => void
}

export default createContext<ModalContext>({ 
  modal: () => {},
  closeModal: () => {},
  updateModal: () => {}
})