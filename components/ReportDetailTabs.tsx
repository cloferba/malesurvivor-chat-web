import Tabs from './Tabs'

interface ReportDetailTabsProps {
  reportId: any,
  path: string,
  page: any
}

export default function ReportDetailTabs({reportId, path, page}: ReportDetailTabsProps) {
  return (
    <Tabs>
      <Tabs.Tab
        href={`/admin/reports/${reportId}?b=${page}`}
        active={path == 'detail'}
      >
        Details
      </Tabs.Tab>
      <Tabs.Tab
        href={`/admin/reports/${reportId}/context?b=${page}`}
        active={path == 'context'}
      >
        Context
      </Tabs.Tab>
      <Tabs.Tab
        href={`/admin/reports/${reportId}/discussion?b=${page}`}
        active={path == 'discussion'}
      >
        Discussion
      </Tabs.Tab>
    </Tabs>
  )
}