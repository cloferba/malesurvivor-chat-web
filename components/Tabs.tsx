import Link from 'next/link'

export default function Tabs({children}) {

  return (
    <div className="flex bg-gray-100 dark:bg-gray-800 rounded overflow-hidden">
      {children}
    </div>
  )
}

function Tab({ href, children, active = false }) {

  const bar = (
    <div className={`${active ? 'h-1' : 'h-0'} w-10  rounded-tl rounded-tr bg-gray-800 dark:bg-gray-100 absolute bottom-0 transition-all group-hover:h-1`}/>
  )

  return (
    <Link href={href} shallow>
      <a className={`flex-1 flex justify-center py-3 relative group`}>
        {children}
        {bar}
      </a>
    </Link>
  )
}

Tabs.Tab = Tab