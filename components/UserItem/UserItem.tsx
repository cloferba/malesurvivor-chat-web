import { STATUS_TYPES } from '../../lib/constants'
import { MdChatBubbleOutline, MdChat } from 'react-icons/md'
import Avatar from '../Avatar'

export default function UserItem({ username, avatar_url_m, status, onSendMd, lowOpacityWhenOffline }) {

  const { iconColor } = STATUS_TYPES[status]
  const isOffline = status == 'offline' ? true : null

  const statusDot = !isOffline && (
    <div
      style={{ backgroundColor: iconColor }}
      className="rounded-full w-3 h-3 absolute -bottom-0.5 right-0"
    />
  )

  const imageStyle = isOffline && lowOpacityWhenOffline ? { filter: 'grayscale(100%)' } : null 

  const rightButton = onSendMd ? (
    <div
      className="ml-auto text-accent p-2 group cursor-pointer"
      onClick={onSendMd}
    >
      <span className="group-hover:hidden">
        <MdChatBubbleOutline size={20}/>
      </span>
      <span className="hidden group-hover:block">
        <MdChat size={20}/>
      </span>
    </div>
  ) : null

  return (
    <div className={`flex items-center px-4 py-2 ${isOffline && lowOpacityWhenOffline ? 'opacity-60' : ''}`}>
      <div className="relative mr-2" style={imageStyle}>
        <Avatar src={avatar_url_m} />
        {statusDot}
      </div>
      {username}
      {rightButton}
    </div>
  )
}