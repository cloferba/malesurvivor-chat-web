import { useState } from 'react'
import setTheme from '../../lib/setTheme'
import ThemeSelector from './ThemeSelector'

export default function ThemeSelectorModal() {

  const [theme, setLocalTheme] = useState(localStorage?.theme || 'auto')

  const handleChange = newTheme => {
    if (newTheme == 'auto') {
      localStorage.removeItem('theme')
    } else {
      localStorage.theme = newTheme
    }
    setTheme()
    setLocalTheme(newTheme)
  }

  return (
    <div className="mt-4">
      <ThemeSelector
        value={theme}
        onChange={handleChange}
      />
    </div>
  )
}