import { MdBrightness5, MdBrightness4, MdBrightness2 } from 'react-icons/md'

const options = [
  {
    key: 'light',
    icon: <MdBrightness5 size={40}/>,
    title: 'Light',
    className: 'dark:bg-gray-700',
  },
  {
    key: 'auto',
    icon: <MdBrightness4 size={40} />,
    title: 'Automatic',
    className: 'border dark:border-gray-500',
  },
  {
    key: 'dark',
    icon: <MdBrightness2 size={40}/>,
    title: 'Dark',
    className: 'bg-gray-600 text-white dark:bg-gray-900',
  }
]

export default function ThemeSelector({ value = 'auto', onChange }) {
  return (
    <div className="flex space-x-4">
      {options.map(option => (
        <div
          key={option.key}
          className={`
            flex-1 text-center rounded-md
            px-3 py-6 cursor-pointer ${option.className}
            ${value == option.key ? 'ring-2 border-transparent' : ''}
          `}
          onClick={() => onChange(option.key)}
        >
          <h2 className="flex justify-center mb-3">
            {option.icon}
          </h2>
          <h4>
            {option.title}
          </h4>
          <p className="opacity-50">
            {option.desc}
          </p>
        </div>
      ))}
    </div>
  )
}