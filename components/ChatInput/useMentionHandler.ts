import { useState, useEffect, useMemo } from 'react'
import { User } from '../../lib/types'

function findCurrentWord(input: HTMLTextAreaElement) {
  let count = 0
  const words = input.value.split(' ')
  for(const word of words) {
    count += word.length + 1
    if(count == input.selectionEnd) return null
    if(count > input.selectionEnd) return word
  }
  return null
}

export default function useMentionHandler(users: Partial<User>[], input: HTMLTextAreaElement) {

  const [highlightIndex, setHighlightIndex] = useState(0)
  const [mentioning, setMentioning] = useState(null)
  const [filterText, setFilterText] = useState('')

  useEffect(() => {
    if(!input) return
    input.addEventListener('keydown', handleInputKeyDown)
    input.addEventListener('keyup', checkIfMentioning)
    input.addEventListener('click', checkIfMentioning)
    return () => {
      input.removeEventListener('keydown', handleInputKeyDown)
      input.removeEventListener('keyup', checkIfMentioning)
      input.removeEventListener('click', checkIfMentioning)
    }
  }, [input, handleInputKeyDown, checkIfMentioning])

  useEffect(() => {
    setHighlightIndex(0)
  }, [filterText])

  const usersToMention = useMemo(() => {
    return users
      .filter(({username}) => username.includes(filterText))
      .sort((a, b) => a.username.localeCompare(b.username))
      .map((user, index) => ({...user, highlighted: highlightIndex == index}))
  }, [users, filterText, highlightIndex])

  function addMention(username) {
    setMentioning(false)
    const word = findCurrentWord(input)
    const restOfUserName = username.slice(word.length - 1) + ' '
    input.setRangeText(restOfUserName, input.selectionStart, input.selectionEnd, "end")
    input.focus()
  }

  function handleInputKeyDown(e: KeyboardEvent) {
    if(!mentioning) return
    e.stopPropagation()
    switch(e.code) {
      case 'ArrowUp':
        e.preventDefault()
        const isFirst = highlightIndex == 0
        setHighlightIndex(isFirst ? usersToMention.length - 1 : highlightIndex - 1)
        break
      case 'ArrowDown':
        e.preventDefault()
        const isLast = highlightIndex == usersToMention.length - 1
        setHighlightIndex(isLast ? 0 : highlightIndex + 1)
        break
      case 'Enter':
        e.preventDefault()
        const selectedUser = usersToMention[highlightIndex]
        selectedUser && addMention(selectedUser.username)
        break
    }
  }

  function checkIfMentioning(e) {
    const word = findCurrentWord(input)
    const startsWithAt = word && word.startsWith('@')
    setMentioning(startsWithAt)
    startsWithAt && setFilterText(word.slice(1))
  }

  return {
    mentioning,
    addMention,
    usersToMention
  }
}