import React, { useEffect, useRef } from 'react'
import IconButton from '../IconButton'
import { MdSend, MdTagFaces } from 'react-icons/md'
import ReplyingTo from './ReplyingTo'
import MentionSelector from './MentionSelector'
import useMentionHandler from './useMentionHandler'
import { ReplyMessage, User } from '../../lib/types'
import dynamic from 'next/dynamic'
import Dropdown from '../Dropdown'
import TextareaAutosize from 'react-textarea-autosize'

const EmojiPicker = dynamic(
  () => import('emoji-picker-react'),
  { ssr: false }
)

interface ChatInputProps {
  roomUsers: Record<User['user_id'], Partial<User>>,
  replyingTo?: ReplyMessage,
  onCancelReplay?: () => void,
  onSend?: (text: string) => void,
  disabled?: boolean,
  extra?: any,
}

const defaultRoomUsers = {}

export default function ChatInput({
  roomUsers = defaultRoomUsers,
  replyingTo,
  onCancelReplay,
  onSend,
  disabled = false,
  extra,
}: ChatInputProps) {

  const inputRef = useRef<HTMLTextAreaElement>(null)
  const users: Partial<User>[] = Object.values(roomUsers)
  const { mentioning, addMention, usersToMention } = useMentionHandler(users, inputRef.current)

  useEffect(() => {
    replyingTo && inputRef.current.focus()
  }, [replyingTo])

  const send = () => {
    if(!inputRef.current.value.length) return
    onSend && onSend(inputRef.current.value)
    inputRef.current.value = ''
    inputRef.current.focus()
  }
  
  const handleKeyDown = e => {
    if(e.keyCode == 13 && !e.shiftKey && !mentioning) {
      e.preventDefault()
      send()
    }
  }

  const onEmojiClick = (e, {emoji}) => {
    const input = inputRef.current
    input.setRangeText(emoji, input.selectionStart, input.selectionEnd, "end")
  }

  const mentionSelector = (
    <MentionSelector
      users={usersToMention}
      onUserClicked={addMention}
    />
  )

  const emojiPicker = (
    <div
      className="relative"
      data-testid="emoji-picker"
    >
      <EmojiPicker
        onEmojiClick={onEmojiClick}
        disableSearchBar
        disableSkinTonePicker
        native
        preload
      />
    </div>
  )

  const emojiButton = (
    <Dropdown
      overlay={emojiPicker}
      placement="bottom"
      offset={48}
    >
      <IconButton
        icon={MdTagFaces}
        data-testid="emoji-button"
      />
    </Dropdown>
  )

  const renderReplyingTo = (
    <ReplyingTo
      onCancel={onCancelReplay}
      {...replyingTo}
    />
  )

  return (
    <div className="px-3 py-2 relative">
      {replyingTo && renderReplyingTo}
      {mentioning && mentionSelector}
      {extra}
      <div className={`flex items-center ${disabled ? 'pointer-events-none opacity-60' : ''}`}>
        {emojiButton}
        <TextareaAutosize
          rows={1}
          maxRows={5}
          ref={inputRef}
          className="bg-gray-200 dark:bg-gray-800 px-4 py-2 rounded-3xl text-lg outline-none w-full flex-1 mr-1 resize-none"
          onKeyDown={handleKeyDown}
          placeholder="Write your message here"
          disabled={disabled}
        />
        <IconButton
          icon={MdSend}
          onClick={send}
          data-testid="send-button"
        />
      </div>
    </div>
  )
}