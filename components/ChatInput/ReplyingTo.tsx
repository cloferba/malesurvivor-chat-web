import IconButton from '../IconButton'
import { MdClose } from 'react-icons/md'

export default function ReplyingTo({
  username,
  text,
  onCancel
}) {
  return (
    <div className="flex items-center mb-2 border-t border-b py-3 dark:border-gray-800">
      <div className="py-2 px-4 bg-gray-200 dark:bg-gray-800 rounded-lg mr-3 flex-1">
        <div className="font-bold opacity-80">
          {username}
        </div>
        <div
          className="img-container"
          dangerouslySetInnerHTML={{__html: text}}
        />
      </div>
      <IconButton
        icon={MdClose}
        onClick={onCancel}
      />
    </div>
  )
}