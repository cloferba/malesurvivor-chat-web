import { useEffect, useRef } from 'react'
import Avatar from '../Avatar'

export default function MentionSelector({ users, onUserClicked }) {

  const userDivRef = useRef()
  const containerRef = useRef()

  useEffect(() => {
    if(userDivRef.current) {
      const container = containerRef.current
      container.scroll({top: userDivRef.current.offsetTop - container.offsetHeight / 2 })
    }
  }, [userDivRef.current])

  return (
    <div
      ref={containerRef}
      className="p-1 rounded-md bg-gray-200 mb-1 absolute bottom-14 max-h-64 overflow-y-auto z-40 shadow-lg"
      data-testid="mention-selector"
    >
      {users.map((user, i) => (
        <div
          key={user.user_id}
          className={`flex items-center hover:bg-gray-100 py-1 w-full pl-2 pr-3 cursor-pointer rounded-sm ${user.highlighted ? 'bg-gray-100' : ''}`}
          onClick={() => onUserClicked(users[i].username)}
          ref={user.highlighted ? userDivRef : null}
        >
          <div className="mr-2">
            <Avatar size="small" src={user.avatar_url_m} />
          </div>
          <div className="font-medium">
            {user.username}
          </div>
        </div>
      ))}
    </div>
  )
}