import { render, fireEvent, waitFor, screen } from '@testing-library/react'
import ChatInput from '.'

const roomUsers = {hhh234: {user_id: 'hhh234', username: 'lucas'}}

const getUtils = () => render(<ChatInput roomUsers={roomUsers}/>)

test('loads and displays greeting', async () => {
  const utils = getUtils()
  const input = utils.getByRole('textbox')
  const sendButton = utils.getByTestId('send-button')

  fireEvent.change(input, { target: { value: 'first' } })
  expect(input.value).toBe('first')

  fireEvent.click(sendButton)
  expect(input).toHaveFocus()
  expect(input.value).toBe('')

  fireEvent.change(input, { target: { value: '@l ' } })
  expect(input.value).toBe('@l ')
  
  fireEvent.click(input, { offsetX: 188, offsetY: 22 })
  expect(input).toHaveFocus()
  // await waitFor(() => utils.getByTestId('mention-selector'))
  // await screen.findByTestId('')

  // expect(screen.getByTestId('mention-selector')).toBeInTheDocument()
})

test('open emojis selector', async () => {
  const utils = getUtils()
  const emojiButton = utils.getByTestId('emoji-button')
  fireEvent.click(emojiButton)
  await screen.findByTestId('emoji-picker')
})