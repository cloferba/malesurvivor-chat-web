import Link from 'next/link'
import { useRouter } from 'next/router'
import { MdOutlineUndo, MdVisibilityOff } from 'react-icons/md'
import { useSetRecoilState } from 'recoil'
import roomsState from '../../atoms/rooms'
import socket from '../../lib/socket'
import SidebarItem from '../SidebarItem'

export default function RoomsList({ rooms, icon: Icon, showHidden = false, }) {

  const { query: { roomId } } = useRouter()
  const setRooms = useSetRecoilState(roomsState)

  const HideIcon = ({ id: roomId, hidden }) => {
    const HideOrShow = hidden ? MdOutlineUndo : MdVisibilityOff
    return (
      <HideOrShow
        size={26}
        className={`
          cursor-pointer p-1 rounded
          hover:bg-white hover:bg-opacity-10
        `}
        onClick={() => {
          socket.emit('toggle-hide-room', roomId, setRooms)
        }}
      />
    )
  }

  return (
    <>
      {rooms.map(room => {
        if (!showHidden && room.hidden) {
          return null
        }
        const label = (
          <>
            <span className="line-clamp-1">
              {room.name}
            </span>
            {room.type != 'u2u' && (
              <span className="opacity-40 ml-2 flex-shrink-0">
                {room.onlineUsersCount || 0}
              </span>
            )}
          </>
        )
        return (
          <div
            key={room.id}
            className="relative flex items-center group"
          >
            <Link
              key={room.id}
              href={`/rooms/${room.id}`}
            >
              <a className={`w-full ${room.hidden ? 'opacity-50' : ''}`}>
                <SidebarItem
                  icon={<Icon/>}
                  label={label}
                  active={roomId == room.id}
                  hasNotification={room.hasNotification}
                />
              </a>
            </Link>
            <div className={`
              absolute right-2.5 ${room.hidden ? '' : 'hidden'} group-hover:block
            `}>
              <HideIcon {...room} />
            </div>
          </div>
        )
      })}
    </>
  )
}