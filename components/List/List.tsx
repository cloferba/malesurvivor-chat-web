import { usePagination } from 'react-use-pagination'
import { MdChevronLeft, MdChevronRight } from 'react-icons/md'
import { useRouter } from 'next/router'
import { useEffect } from 'react'

const getValidPage = page => {
  if(!page) return 0
  if(isNaN(page)) return 0
  return Number(page)
}

export default function List({data, children, loading = false}) {

  const router = useRouter()
  const { query: { page } } = router

  const initialPage = getValidPage(page)

  const {
    currentPage, 
    totalPages, 
    setNextPage, 
    setPreviousPage,
    nextEnabled,
    previousEnabled,
    startIndex,
    endIndex,
    setPage,
  } = usePagination({ totalItems: data.length, initialPage, initialPageSize: 10 })

  useEffect(() => {
    setPage(initialPage)
  }, [initialPage])

  const previous = () => {
    router.push({ query: { ...router.query, page: currentPage - 1}})
    setPreviousPage()
  }

  const next = () => {
    setNextPage()
    router.push({ query: { ...router.query, page: currentPage + 1 }})
  }

  const slicedData = data.slice(startIndex, endIndex + 1)
  
  return (
    <>
      {loading && (
        <div className="opacity-50 text-center py-6">
          Loading...
        </div>
      )}

      {!slicedData.length && !loading && (
        <div className="opacity-50 text-center py-6">
          No records yet
        </div>
      )}

      {slicedData.map((item, i) => children(item, i, currentPage))}

      <div className="flex items-center justify-center space-x-3 pt-5">
        <button
          className="rounded border disabled:opacity-50 disabled:cursor-auto"
          onClick={previous}
          disabled={!previousEnabled}
        >
          <MdChevronLeft size={34} opacity={0.7}/>
        </button>
        <span>page {currentPage + 1} of {totalPages}</span>
        <button
          className="rounded border disabled:opacity-50 disabled:cursor-auto"
          onClick={next}
          disabled={!nextEnabled}
        >
          <MdChevronRight size={34} opacity={0.7}/>
        </button>
      </div>
    </>
  )
}