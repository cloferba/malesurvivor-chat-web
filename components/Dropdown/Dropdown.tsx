import { useEffect, useRef, useState } from 'react'

interface DropdownProps {
  overlay?: any,
  children: any,
  placement?: 'left' | 'right' | 'top' | 'bottom',
  onOpenChange?: (open: any) => void,
  className?: any,
  openOnHover?: boolean,
  open?: boolean,
  preventClosingOnLeave?: boolean,
  offset?: number,
  disabled?: boolean,
}

export default function Dropdown({
  overlay,
  children,
  placement = 'left',
  onOpenChange,
  className = '',
  openOnHover,
  open: initialOpen = false,
  preventClosingOnLeave = false,
  offset = 0,
  disabled = false,
  ...props
}: DropdownProps) {

  const [open, setOpen] = useState(initialOpen)
  useEffect(() => setOpen(initialOpen), [initialOpen])

  const dropdownRef = useRef(null)
  const mouseoutTimeout = useRef()
  const eventTimeout = useRef(null)

  const updateOpen = (value = false) => {
    if(eventTimeout.current) return
    setOpen(value)
    onOpenChange && onOpenChange(value)
  }

  const handleMouseEnter = e => {
    clearTimeout(mouseoutTimeout.current) // prevent closing
    openOnHover && updateOpen(true)
  }
  
  const handleMouseLeave = e => {
    if(!preventClosingOnLeave) {
      // @ts-ignore - close only if the user has left the dropdown for 150ms 
      mouseoutTimeout.current = setTimeout(updateOpen, 150)
    }
  }

  const setListeners = (target, action) => {
    target[action]('mouseenter', handleMouseEnter)
    target[action]('mouseleave', handleMouseLeave)
  }

  useEffect(() => {
    const dropdownEl = dropdownRef.current
    setListeners(dropdownEl, 'addEventListener')
    return () => setListeners(dropdownEl, 'removeEventListener')
  }, [setListeners])

  useEffect(() => {
    function clearEventTimeout() {
      clearTimeout(eventTimeout.current)
      eventTimeout.current = null
    }
    if(!open) {
      clearEventTimeout
    } else {
      eventTimeout.current = setTimeout(clearEventTimeout, 150)
    }
  }, [open])

  const toggle = () => {
    updateOpen(!open)
  }

  if(typeof children == 'function') {
    return (
      <div ref={dropdownRef}>
        {children(open, toggle)}
      </div>
    )
  }

  return (
    <div
      className={`relative ${className}`}
      ref={dropdownRef}
      {...props}
    >
      <span onClick={toggle}>
        {children}
      </span>
      {open && (
        <div
          className="absolute z-20"
          style={{
            [placement]: offset
          }}
        >
          {overlay}
        </div>
      )}
    </div>
  )
}