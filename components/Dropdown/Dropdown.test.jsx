import { render, fireEvent, screen, waitForElementToBeRemoved } from '@testing-library/react'
import Dropdown from '.'

const menu = <div data-testid="overlay"/>
const button = <button data-testid="trigger" />

test('open and close default dropdown', async () => {
  render(
    <Dropdown
      overlay={menu}
      data-testid="dropdown"
    >
      {button}
    </Dropdown>
  )
  const trigger = screen.getByTestId('trigger')
  const dropdown = screen.getByTestId('dropdown')

  // check overlay doesn't exists
  const overlay = screen.queryByTestId('overlay')
  expect(overlay).toBeNull()

  // open with click
  fireEvent.click(trigger.parentElement)
  await screen.findByTestId('overlay')

  // close with mouseleave
  fireEvent.mouseLeave(dropdown)
  await waitForElementToBeRemoved(() => screen.queryByTestId('overlay'))
})

test('open and close dropdown with hover', async () => {
  render(
    <Dropdown
      overlay={menu}
      openOnHover
      data-testid="dropdown"
    />
  )
  const dropdown = screen.getByTestId('dropdown')

  // open with mouseenter
  fireEvent.mouseEnter(dropdown)
  await screen.findByTestId('overlay')

  // close with mouseleave
  fireEvent.mouseLeave(dropdown)
  await waitForElementToBeRemoved(() => screen.queryByTestId('overlay'))
})