import { useState } from 'react'
import { typesOfBanParsed } from '../../lib/constants'
import socket from '../../lib/socket'
import DropdownSelect from '../DropdownSelect'

const banTimes = {
  threeDays: '3 days',
  twoWeeks: '2 weeks',
  oneMonth: '1 month',
  permanent: 'Permanent'
}

const typeOptions = Object.keys(typesOfBanParsed).map(type => ({ 
  value: type,
  label: typesOfBanParsed[type]
}))

const BanUser = ({ userId, closeModal }) => {
  const [duration, setDuration] = useState('threeDays')
  const [type, setType] = useState('generalBan')
  
  const onBanConfirmed = () => {
    socket.emit('ban-user', { duration, type, userId }, ({ data, error }) => {
      closeModal()
    })
  }

  return (
    <div className="mt-3">
      <div className="mb-4">
        <div className="font-medium mb-2" >
          Type of ban:
        </div>
        <DropdownSelect
          options={typeOptions}
          value={type}
          onChange={setType}
        />
      </div>
      <div className="font-medium mb-2" >
        Time:
      </div>
      <div>
        {Object.keys(banTimes).map(key => (
          <div className="flex" key={key}>
            <label className="p-2 flex items-center">
              <input name="timeCheckbox" type="radio" value={key} onChange={({ target }) => setDuration(target.value)} />
              <div className="pl-1">{banTimes[key]}</div>
            </label>
          </div>
        ))}
      </div>
      <div className="mt-7 text-right">
        <button
          className="btn btn-primary text-right"
          onClick={onBanConfirmed}
        >
          Confirm
        </button>
      </div>
    </div>
  )
}

export default BanUser