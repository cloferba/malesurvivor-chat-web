import Link from 'next/link'
import { FORUM_NAME, FORUM_LOGIN_URL } from '../lib/constants'

export const mapErrors = {
  'no-user': {
    title: `You are not logged in`,
    desc: (
      <>
        <div className="text-center mt-2">
          You first need to&nbsp;
          <Link href={FORUM_LOGIN_URL}>
            <a target="_blank" className="link">
              log in
            </a>
          </Link>
          &nbsp;to {FORUM_NAME}, then come back to this page and reload
        </div>
        <div className="text-right mt-5">
          <button
            onClick={() => window.location.reload()}
            className="btn btn-primary"
          >
            Reload
          </button>
        </div>
      </>
    ),
    hideDefaultButton: true
  },
  'invalid-user': {
    title: `You don't have permissions to use this chat`,
    desc: (
      <div className="text-center mt-3">
        Please contact a moderator for more information
      </div>
    ),
    hideDefaultButton: true
  }
}