import { motion, Variants } from 'framer-motion'
import { useEffect, useState } from 'react'

const variants: Variants = {
  hidden: { y: 30, opacity: 0 },
  visible: { y: 43, opacity: 1 }
}

interface TooltipProps {
  children: any,
  text: string,
  onVisibleChange?: (visible: boolean) => void,
  visible?: boolean
}

export default function Tooltip({children, text, onVisibleChange, visible }: TooltipProps) {

  const [hovered, setHovered] = useState(false)
  const onMouseEnter = () => setHovered(true)
  const onMouseLeave = () => setHovered(false)
  const spanProps = { onMouseEnter, onMouseLeave }
  const finalVisible = typeof visible != 'undefined' ? visible : hovered

  useEffect(() => {
    onVisibleChange && onVisibleChange(finalVisible)
  }, [finalVisible])

  return (
    <span className="relative flex justify-center" {...spanProps}>
      {children}
      {finalVisible && (
        <motion.div
          initial="hidden"
          animate="visible"
          variants={variants}
          className="absolute pointer-events-none px-2 bg-gray-600 text-white rounded z-10 mt-1 max-w-xs"
        >
          {text}
        </motion.div>
      )}
    </span>
  )
}