const sizesClasses = {
  small: 'h-7 w-7',
  middle: 'h-10 w-10',
  large: 'h-12 w-12'
}

interface AvatarProps {
  src: string,
  size?: 'small' | 'middle' | 'large'
}

export default function Avatar({src, size = 'middle'}: AvatarProps) {
  return (
    <div className={`${sizesClasses[size]} rounded-full overflow-hidden bg-gray-400 bg-opacity-20`}>
      <img src={src}/>
    </div>
  )
}