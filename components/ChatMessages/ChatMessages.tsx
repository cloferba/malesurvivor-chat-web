import { forwardRef, useRef, useState } from 'react'
import useInfiniteScroll from '../../components/useInfiniteScroll'
import { Message } from '../../lib/types'
import { dateDiffInMinutes, getOnlyDate, sameDay } from '../../lib/utils'
import Divider from './Divider'

interface ChatMessagesProps {
  loading?: boolean,
  messages: Message[],
  onGetPrevious?: (cb) => void,
  onGetFollowing?: (cb) => void,
  children: (message, i) => any
}

const LoadingToast = ({ placement = 'top' }) => (
  <div className={`py-2 absolute  ${placement == 'bottom' ? 'bottom-0' : '' } w-full flex justify-center z-10 bg-gray-400 bg-opacity-20`}>
    <div className="rounded bg-gray-50 inline-block px-3 py-0.5 shadow">
      Loading...
    </div>
  </div>
)

const getHasPreviousOrFollowing = (message, compareMessage) => {
  if(!message || !compareMessage) return false
  const isSameThanLast = compareMessage.user_id == message.user_id
  const prevMessageDifference = dateDiffInMinutes(compareMessage.createdAt, message.createdAt)
  return isSameThanLast && prevMessageDifference < 30
}

const ChatMessages = forwardRef(({
  messages,
  onGetPrevious,
  onGetFollowing,
  children
}: ChatMessagesProps, ref: any) => {

  const fallbackRef = useRef()
  const [loadingPrevious, setLoadingPrevious] = useState(false)
  const [loadingFollowing, setLoadingFollowing] = useState(false)

  const onTapTop = cb => {
    if(!onGetPrevious || loadingPrevious) return
    setLoadingPrevious(true)
    onGetPrevious(() => {
      setLoadingPrevious(false)
      cb()
    })
  }
  
  const onTapBottom = () => {
    if(!onGetFollowing || loadingFollowing) return
    setLoadingFollowing(true)
    onGetFollowing(() => {
      setLoadingFollowing(false)
    })
  }

  useInfiniteScroll({
    onTapTop,
    onTapBottom,
    scrollTarget: ref ? ref.current : fallbackRef.current,
  })

  return (
    <>
      <div
        className="overflow-y-auto relative h-full"
        ref={ref || fallbackRef}
      >
        {loadingPrevious && <LoadingToast/>}
        {messages.map((message, i) => {
          const hasPrevious = getHasPreviousOrFollowing(message, messages[i - 1])
          const hasFollowing = getHasPreviousOrFollowing(messages[i + 1], message)
          const isFirstOfTheDay = !messages[i - 1] || !sameDay(message.createdAt, messages[i - 1].createdAt)
          return (
            <span key={message.id}>
              {isFirstOfTheDay && <Divider text={getOnlyDate(message.createdAt)} />}
              {children({ ...message, hasPrevious, hasFollowing }, i)}
            </span>
          )
        })}
      </div>
      {loadingFollowing && <LoadingToast placement="bottom"/>}
    </>
  )
})

export default ChatMessages