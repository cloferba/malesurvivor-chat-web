const line = (
  <div
    style={{height: 1}}
    className="flex-1 bg-gray-200 dark:bg-gray-500 h-0.5"
  />
)

export default function Divider({ text }) {
  return (
    <div className="my-1 flex items-center mx-3">
      {line}
      <div className="flex-grow-0 bg-gray-100 dark:bg-gray-700 px-2 text-sm opacity-70 rounded border dark:border-gray-500" >
        {text}
      </div>
      {line}
    </div>
  )
}
