import { useContext } from 'react'
import { MdMenu } from 'react-icons/md'
import { useRecoilValue } from 'recoil'
import userState from '../../atoms/user'
import layoutContext from '../../containers/layoutContext'
import { User } from '../../lib/types'
import IconButton from '../IconButton'
import NotisDropdown from '../Notifications/NotisDropdown'

interface TopBarProps {
  title: any,
  extra?: JSX.Element | JSX.Element[],
  leftExtra?: JSX.Element | JSX.Element[],
  darkMode?: boolean,
}

export default function TopBar({ title, leftExtra, extra, darkMode }: TopBarProps) {

  const user = useRecoilValue<User>(userState)
  const { setMenuVisible } = useContext(layoutContext)

  if(!user) return null

  const leftButton = (
    <IconButton
      darkMode={darkMode}
      className="block lg:hidden mr-3"
      onClick={() => setMenuVisible(true)}
      icon={MdMenu}
    />
  )

  return (
    <div className={`px-2 md:px-4 dark:bg-gray-800 ${darkMode ? 'bg-gray-800' : 'bg-gray-100'} flex items-center border-r dark:border-gray-700`}>
      {leftButton}
      <h4 className={`py-3 ${darkMode ? 'text-white' : null} line-clamp-1`}>
        {title}
      </h4>
      <div className="ml-auto flex items-center">
        {leftExtra}
        <NotisDropdown darkMode={darkMode}/>
        {extra}
      </div>
    </div>
  )
}