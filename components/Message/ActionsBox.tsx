import { useState } from 'react'
import { MdReply, MdFlag, MdMoreVert, MdVisibilityOff, MdOpenInNew, MdCancel, MdDisabledVisible } from 'react-icons/md'
import { FORUM_URL } from '../../lib/constants'
import { Message } from '../../lib/types'
import Dropdown from '../Dropdown'

function ActionIcon({ icon: Icon, onClick = () => {} }) {
  return (
    <div
      className="cursor-pointer rounded-sm hover:bg-gray-200 dark:hover:bg-gray-700 p-1"
      onClick={onClick}
    >
      <Icon size={22}/>
    </div>
  )
}

interface ActionsBoxProps {
  onReply?: () => void,
  onReport?: () => void,
  message?: Message,
  onCensorMessage?: () => void,
  onCreateReport?: () => void,
  isDifferentUser?: boolean,
  onBanUser?: () => void,
  onIgnoreUser: () => void,
}

export default function ActionsBox({
  onReply,
  message,
  onCensorMessage,
  onCreateReport,
  isDifferentUser,
  onBanUser,
  onIgnoreUser,
}: ActionsBoxProps) {

  const [open, setOpen] = useState(false)

  const censorMessage = () => {
    onCensorMessage()
    setOpen(false)
  }

  const createReport = () => {
    onCreateReport()
    setOpen(false)
  }

  const actionClass = "menu-item p-2 hover:bg-gray-100 dark:hover:bg-gray-600"

  const adminOptionsMenu = (
    <div className="bg-gray-50 dark:bg-gray-700 w-44 rounded overflow-hidden shadow-md relative -bottom-2">
      {onCensorMessage ? (
        <div
          className={`${actionClass} ${message.censored ? 'disabled' : ''}`}
          onClick={censorMessage}
        >
          <MdVisibilityOff />
          <span>Censor message</span>
        </div>
      ) : null }
      {isDifferentUser && (
        <>
          <a
            target="_blank"
            href={`${FORUM_URL}members/${message.user_id}`}
            className={actionClass}
          >
            <MdOpenInNew />
            <span>View profile</span>
          </a>
          <div
            onClick={onIgnoreUser}
            className={actionClass}
          >
            <MdDisabledVisible />
            <span>Ignore user</span>
          </div>
          {Boolean(onBanUser) && (
            <div className={actionClass} onClick={onBanUser}>
              <MdCancel />
              <span>Ban</span>
            </div>
          )}
        </>
      )}
    </div>
  )

  const moreOptions = Boolean(onCensorMessage || isDifferentUser) && (
    <Dropdown
      overlay={adminOptionsMenu}
      placement="right"
      onOpenChange={setOpen}
      open={open}
    >
      <ActionIcon icon={MdMoreVert} />
    </Dropdown>
  )

  return (
    <div
      className="rounded-md bg-gray-50 dark:bg-gray-800 border dark:border-gray-600 p-1 flex space-x-1"
    >
      <ActionIcon
        onClick={onReply}
        icon={MdReply}
      />
      {Boolean(onCreateReport) && (
        <ActionIcon
          onClick={createReport}
          icon={MdFlag}
        />
      )}
      {moreOptions}
    </div>
  )
}