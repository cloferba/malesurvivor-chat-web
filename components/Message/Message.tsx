import { Ref, useCallback } from 'react'
import { ReplyMessage } from '../../lib/types'
import { getDateTime, isToday, getDateAndTime } from '../../lib/utils'
import Avatar from '../Avatar'

interface MessageProps {
  text: any,
  createdAt: Date,
  censored: boolean,
  username: string,
  avatar_url_m: string,
  replyingTo: ReplyMessage,
  hasPrevious?: boolean,
  hasFollowing?: boolean,
  highlighted?: boolean,
  id: string,
  innerRef?: Ref<HTMLDivElement>,
  actionsBox?: any,
  currentUsername?: string
}

export default function Message({
  text,
  createdAt,
  censored,
  username,
  avatar_url_m,
  replyingTo,
  hasPrevious,
  hasFollowing,
  highlighted,
  id,
  innerRef,
  actionsBox,
  currentUsername
}: MessageProps) {

  const date = isToday(createdAt) ? getDateTime(createdAt) : getDateAndTime(createdAt)
  const onlyTime = getDateTime(createdAt)
  const htmlText = text
  const htmlReplyingTo = replyingTo?.text

  const messageRef = useCallback((node: HTMLElement) => {
    if (node !== null) {
      const mentions = node.getElementsByClassName('mention-span')
      for (const mention of mentions) {
        if (mention.textContent.includes(currentUsername)) {
          mention.setAttribute('class', 'mention-span-me')
        }
      }
    }
  }, [])
  
  const renderedReplyingTo = replyingTo && (
    <div className="px-2 py-1 my-1 rounded-md bg-gray-100 dark:bg-gray-800 border bg-opacity-60 dark:border-gray-600">
      <div>
        <span className="font-bold opacity-80">
          {replyingTo.username}
        </span>
      </div>
      <div
        className="text-sm img-container"
        dangerouslySetInnerHTML={{__html: htmlReplyingTo }}
        ref={messageRef}
      />
    </div>
  )

  const renderedDate = (
    <span className="ml-2 text-xs opacity-70">
      {date}
    </span>
  )

  const avatarOrDate = !hasPrevious ? (
    <Avatar src={avatar_url_m} />
  ) : (
    <div className="hidden group-hover:block relative">
      <span className="ml-2 text-xs opacity-70 absolute -right-1 top-1 whitespace-nowrap">
        {onlyTime}
      </span>
    </div>
  )

  const renderedUsername = !hasPrevious && (
    <div>
      <span className="font-bold opacity-80">
        {username}
      </span>
      {renderedDate}
    </div>
  )

  return (
    <div
      id={id}
      className={`
        message flex group hover:bg-gray-100 dark:hover:bg-gray-800 relative px-4
        ${hasPrevious ? 'pt-0.5' : 'pt-3'}
        ${hasFollowing ? 'pb-0.5' : 'pb-3'}
        ${highlighted ? 'bg-yellow-100 dark:bg-yellow-600' : ''}
      `}
      ref={innerRef}
    >
      <div className="w-10 flex-shrink-0 mr-3">
        {avatarOrDate}
      </div>
      <div>
        {renderedUsername}
        {renderedReplyingTo}
        <div
          className={`img-container ${censored ? 'opacity-50' : ''}`}
          ref={messageRef}
          dangerouslySetInnerHTML={{ __html: htmlText }}
        />
      </div>
      <div className="hidden group-hover:block absolute right-3 -top-2">
        { actionsBox }
      </div>
    </div>
  )
}