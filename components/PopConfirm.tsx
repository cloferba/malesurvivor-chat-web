import { useState } from 'react'
import Dropdown from './Dropdown'

interface PopConfirmProps {
  children: any,
  cancelText?: string,
  okText?: string,
  title: string,
  desc?: string,
  onConfirm: () => void,
  onCancel?: () => void
}

export default function PopConfirm({
  children,
  cancelText = 'Cancel',
  okText = 'Ok',
  title,
  desc,
  onConfirm,
  onCancel,
}: PopConfirmProps) {

  const [opened, setOpened] = useState(false)

  const modal = (
    <div
      className="shadow p-3 bg-gray-50 -mt-24 rounded max-w-xs"
    >
      <div className="font-semibold">
        {title}
      </div>
      {desc && (
        <div className="text-sm opacity-80">
          {desc}
        </div>
      )}
      <div className="flex items-center mt-3 justify-end">
        <button
          className="btn-small"
          onClick={ onCancel
            ? onCancel
            : () => setOpened(false)
          }
        >
          {cancelText}
        </button>
        <button
          className="btn-small btn-primary ml-2"
          onClick={onConfirm}
        >
          {okText}
        </button>
      </div>
    </div>
  )

  return (
    <div className="relative">
      <Dropdown
        open={opened}
        overlay={modal}
        onOpenChange={setOpened}
        placement="top"
      >
        {children}
      </Dropdown>
    </div> 
  )
  
}