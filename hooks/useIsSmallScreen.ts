import { useEffect, useState } from 'react'

export default function useIsSmallScreen(breakpoint = 1024) {
  const [smallScreen, setSmallScreen] = useState(true)

  useEffect(() => {
    if(window.innerWidth >= breakpoint) setSmallScreen(false)
  }, [])

  return smallScreen
}