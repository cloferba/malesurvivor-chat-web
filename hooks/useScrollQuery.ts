import { useEffect, useState } from 'react'

export default function useScrollQuery(breakPoint, target) {

  const [match, setMatch] = useState(false)

  const handleScroll = e => {
    const scrollTop = target.scrollTop
    if(scrollTop > breakPoint && !match) {
      setMatch(true)
    }
    if(scrollTop <= breakPoint && match) {
      setMatch(false)
    }
  }

  useEffect(() => {
    if(!target) return
    target.addEventListener('scroll', handleScroll)
    return () => target.removeEventListener('scroll', handleScroll)
  }, [match, target, breakPoint])

  return match
}