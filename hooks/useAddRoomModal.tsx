import { useState } from 'react'
import socket from '../lib/socket'
import { useModal } from '../components/Modal'
import { ModalOptions } from '../components/Modal/types'
import DropdownSelect from '../components/DropdownSelect'
import { USER_GROUPS } from '../lib/constants'
import { Room } from '../lib/types'

function validateRoomName(name) {
  if(name.length < 3) return false
  if(name.length > 25) return false
  return true
}

const initialRoom = {
  name: '',
  type: 'public',
  disabled: false,
}

const options = [
  {label: 'Public', value: 'public'},
  {label: 'Private', value: 'staff'},
  {label: 'Link only', value: 'link-only'}
]

function getAllowedGroups(groupsArr) {
  if(!groupsArr) return { '3': true }
  return groupsArr.reduce((acc, group) => ({...acc, [group]: true}), {})
}

function ModalContent({ onSaved, onError, onCancel, roomToEdit }) {

  const defaultRoom = roomToEdit || initialRoom
  const defaultAllowedGroups = getAllowedGroups(defaultRoom.allowed_groups)
  const [newRoomName, setNewRoomName] = useState<string>(defaultRoom.name)
  const [saving, setSaving] = useState(false)
  const [type, setType] = useState(defaultRoom.type)
  const [allowedGroups, setAllowedUserGroups] = useState(defaultAllowedGroups)

  function updateAllowedGroups(groupId, allowed) {
    setAllowedUserGroups(currentAllowedGroups => ({
      ...currentAllowedGroups,
      [groupId]: allowed
    }))
  }

  function saveRoom() {
    const name = newRoomName.trim().trimStart()
    if(!validateRoomName(name)) return
    setSaving(true)
    const allowed_groups = Object.keys(allowedGroups).filter(groupId => allowedGroups[groupId])
    const event = roomToEdit ? 'update-room' : 'create-room'
    const newRoom = type != 'staff' ? { name, type } : { name, type, allowed_groups }
    const cb = ({error, data}) => {
      if(!error) {
        onSaved(data, Boolean(roomToEdit))
      } else {
        onError()
      }
    }
    let args = [newRoom, cb]
    if(roomToEdit) args.splice(0, 0, roomToEdit.id)
    socket.emit(event, ...args)
  }

  const privateCheckboxes = (
    <div className="mt-4">
      {USER_GROUPS.map(({label, value, disabled}) => (
        <div className="my-1" key={value}>
          <label>
            <input
              type="checkbox"
              checked={Boolean(allowedGroups[value])}
              disabled={disabled}
              onChange={e => updateAllowedGroups(value, e.target.checked)}
            />
            {label}
          </label>
        </div>
      ))}
    </div>
  )

  return (
    <div>
      <div className="mt-4">
        <label>Room name:</label>
        <input
          className="form-control w-full mt-2 mb-3"
          value={newRoomName}
          onChange={e => setNewRoomName(e.target.value)}
          disabled={saving}
          placeholder="Write the name here"
        />
      </div>
      <div>
        <DropdownSelect
          value={type}
          options={options}
          onChange={setType}
        />
      </div>
      {type == 'staff' && privateCheckboxes}
      <div className="flex justify-end items-center space-x-3 mt-24">
        <button
          className="btn border"
          disabled={saving}
          onClick={onCancel}
        >
          Cancel
        </button>
        <button
          className="btn btn-primary"
          disabled={saving}
          onClick={saveRoom}
        >
          Save
        </button>
      </div>
    </div>
  )
}

export default function useAddRoomModal(setData) {

  const { modal, closeModal } = useModal()

  function handleError() {
    closeModal()
    modal({title: 'There was an error trying to create the room'})
  }

  function handleSaved(room: Partial<Room>, editing: boolean) {
    if(!editing) {
      setData(currentRooms => ({
        [room.id]: room,
        ...currentRooms
      }))
    } else {
      setData(currentRooms => {
        let result = {...currentRooms}
        result[room.id] = room
        return result
      })
    }
    closeModal()
  }

  const getDesc = roomToEdit => (
    <ModalContent
      onCancel={() => closeModal()}
      onSaved={handleSaved}
      onError={handleError}
      roomToEdit={roomToEdit}
    />
  )

  return (roomToEdit = null) => {
    const title = roomToEdit ? roomToEdit.name : 'Add new room'
    const modalProps: ModalOptions = {
      title,
      desc: getDesc(roomToEdit),
      hideDefaultButton: true,
    }
    modal(modalProps)
  }
}