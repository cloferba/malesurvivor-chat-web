import { useState } from 'react'

export default function useForceRender() {
  const [value, setValue] = useState(false)
  return () => setValue(!value)
}