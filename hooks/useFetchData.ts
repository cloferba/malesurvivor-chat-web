import { useEffect, useState } from 'react'
import { useRecoilValue } from 'recoil'
import connectedState from '../atoms/connected'
import socket from '../lib/socket'

export default function useFetchData(event, defaultData, ...args) {

  const connected = useRecoilValue(connectedState)
  const [data, setData] = useState(defaultData)
  const [unauthorized, setUnauthorized] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    connected && socket.emit(event, ...args, ({data, error}) => {
      if(error == 'unauthorized') setUnauthorized(true)
      data && setData(data)
      setLoading(false)
    })
  }, [connected])

  return { data, unauthorized, loading, setData }
}