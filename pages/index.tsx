import Head from 'next/head'
import TopBar from '../components/TopBar'
import RoomsList from '../containers/RoomsList'
import { APP_NAME } from '../lib/constants'

export default function Home() {
  return (
    <>
      <Head>
        <title>Home - {APP_NAME}</title>
      </Head>
      <div className="h-full grid grid-rows-max-c-auto">
        <TopBar title="Public rooms"/>
        <div className="p-3 lg:px-8 overflow-y-auto">
          <div className="max-w-md">
            <RoomsList />
          </div>
        </div>
      </div>
    </>
  )
}
