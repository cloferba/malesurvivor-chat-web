import List from '../components/List'
import useFetchData from '../hooks/useFetchData'
import { Notification } from '../lib/types'
import NotificationItem from '../components/Notifications/NotificationItem'
import TopBar from '../components/TopBar'
import Head from 'next/head'
import { APP_NAME } from '../lib/constants'

export default function NotificationsPage() {

  const { data: notifications, loading } = useFetchData('get-all-notifications', [])

  return (
    <>
      <Head>
        <title>Notifications - {APP_NAME}</title>
      </Head>
      <div className="h-full grid grid-rows-max-c-auto">
        <TopBar title="Notifications"/>
        <div className="p-8 overflow-y-auto">
          <div className="max-w-3xl divide-y divide-solid">
            <List data={notifications} loading={loading}>
              {(notification: Notification) => (
                <NotificationItem
                  key={notification.id}
                  {...notification}
                />
              )}
            </List>
          </div>
        </div>
      </div>
    </>
  )
}
