import '../styles/globals.css'
import { useEffect } from 'react'
import socket from '../lib/socket'
import { RecoilRoot, useRecoilState, useSetRecoilState } from 'recoil'
import userState from '../atoms/user'
import connectedState from '../atoms/connected'
import { Provider as ModalProvider, useModal } from '../components/Modal' 
import { useRouter } from 'next/router'
import Head from 'next/head'
import DefaultLayout from '../containers/DefaultLayout'
import { APP_NAME } from '../lib/constants'
import initFCM from '../firebase/messaging'
import { mapErrors } from '../components/LoginError'

function Connection() {

  const setUser = useSetRecoilState(userState)
  const [connected, setConnected] = useRecoilState(connectedState)
  const { modal } = useModal()

  useEffect(() => {
    connected && initFCM()
  }, [connected])

  useEffect(() => {
    socket.connect(setConnected)

    socket.on('user/set', user => {
      setConnected(Boolean(user))
      setUser(user)
    })

    socket.on('disconnect', () => {
      window.location.reload()
    })

    socket.on('user/error', err => {
      modal(mapErrors[err])
      console.warn(err)
    })
    
    socket.on('user/update', updateObj => setUser(currentUser => ({
      ...currentUser,
      ...updateObj
    })))
  }, [])

  return null
}

function MyApp({ Component, pageProps }) {

    const router = useRouter()
  
    useEffect(() => {
      const handleRouteChange = url => {
        if(!url.includes('room') && !url.includes('discussion')) {
          socket.emit('leave-room')
        }
      }
  
      router.events.on('routeChangeStart', handleRouteChange)
  
      return () => {
        router.events.off('routeChangeStart', handleRouteChange)
      }
    }, [])

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>{APP_NAME}</title>
      </Head>
      <RecoilRoot>
        <ModalProvider>
          <Connection/>
          <DefaultLayout>
            <Component {...pageProps} />
          </DefaultLayout>
        </ModalProvider>
      </RecoilRoot>
    </>
  )
}

export default MyApp
