import Link from 'next/link'
import { useState } from 'react'
import useConfirmModal from '../../components/ConfirmModal'
import List from '../../components/List'
import { useModal } from '../../components/Modal'
import AdminLayout from '../../containers/AdminLayout'
import useFetchData from '../../hooks/useFetchData'
import socket from '../../lib/socket'
import { Keyword } from '../../lib/types'

export default function KeywordsPage() {

  const { data: keywords, loading, setData, unauthorized } = useFetchData('get-keywords', {})
  const [addingKeyword, setAddingKeyword] = useState(false)
  const [savingNewKeyword, setSavingNewKeyword] = useState(false)
  const [newKeyword, setNewKeyword] = useState<string>('')
  const { openConfirm, closeConfirm } = useConfirmModal()
  const { modal } = useModal()

  const validateKeyword = (keyword) => {
    if(keyword.length < 3) return false
    if(keyword.length > 25) return false
    return true
  }

  const saveKeyword = () => {
    const keyword = newKeyword.trim().trimStart()
    if(!validateKeyword(keyword)) return
    setSavingNewKeyword(true)
    socket.emit('add-keyword', keyword, ({error, data}) => {
      setSavingNewKeyword(false)
      if(error) {
        if(error == 'duplicated_keyword') {
          modal({title: 'This keyword already exists'})
        }
        return
      }
      setData(currentKeywords => ({
        [data.id]: data,
        ...currentKeywords
      }))
      setNewKeyword('')
    })
  }

  const handleKeydown = e => {
    e.keyCode == 13 && saveKeyword()
  }

  const topBar = (
    <div className="flex items-center justify-end space-x-2 my-2">
      {!addingKeyword ? (
        <button
          className="btn btn-primary"
          onClick={() => setAddingKeyword(true)}
        >
          Add keyword
        </button>
      ) : (
        <>
          <input
            className="form-control"
            value={newKeyword}
            onChange={e => setNewKeyword(e.target.value)}
            onKeyDown={handleKeydown}
            disabled={savingNewKeyword}
            placeholder="New keyword here"
          />
          <button
            className="btn btn-primary"
            disabled={savingNewKeyword}
            onClick={saveKeyword}
          >
            Save
          </button>
          <button
            className="btn border"
            disabled={savingNewKeyword}
            onClick={() => setAddingKeyword(false)}
          >
            Cancel
          </button>
        </>
      )}
    </div>
  )

  const removeKeyword = (keywordId) => {
    socket.emit('remove-keyword', keywordId, () => {
      closeConfirm()
      setData(currentKeywords => {
        let result = {...currentKeywords}
        delete result[keywordId]
        return result
      })
    })
  }

  const removeKeywordClicked = ({text, id}) => {
    const title = 'Remove keyword'
    const desc = `Are you sure you want to remove "${text}"?`
    const onConfirm = () => removeKeyword(id)
    openConfirm({title, desc, confirmText: 'Remove', onConfirm, waitAfterConfirm: true })
  }

  return (
    <AdminLayout title="Keywords" unauthorized={unauthorized}>
      {topBar}
      <h4 className="flex py-2 px-2">
        <div className="flex-1">
          Match
        </div>
        <div className="flex-grow-0">
          Incidences
        </div>
        <div className="flex-1 text-right" />
      </h4>
      <List
        data={Object.values(keywords)}
        loading={loading}
      >
        {(keyword: Keyword) => (
          <div
            key={keyword.id}
            className="flex items-center py-2 even:bg-gray-50 dark:even:bg-gray-800 px-2"
          >
            <div className="flex-1">
              {keyword.text}
            </div>
            <div className="w-6 flex-grow-0 text-center">
              <Link href={`/admin/reports?message=${keyword.text}`}>
                <a className="link">
                  {keyword.incidences}
                </a>
              </Link>
            </div>
            <div className="flex-1 text-right">
              <button
                className="btn-small border"
                onClick={() => removeKeywordClicked(keyword)}
              >
                Remove
              </button>
            </div>
          </div>
        )}
      </List>
    </AdminLayout>
  )
}
