import { MdTune } from 'react-icons/md'
import FiltersSelector from '../../components/FiltersSelector'
import List from '../../components/List'
import { useModal } from '../../components/Modal'
import { ModalOptions } from '../../components/Modal/types'
import AdminLayout from '../../containers/AdminLayout'
import useFetchData from '../../hooks/useFetchData'
import { User } from '../../lib/types'
import { getDateAndTime } from '../../lib/utils'
import { typesOfBanParsed } from '../../lib/constants'
import socket from '../../lib/socket'
import useConfirmModal from '../../components/ConfirmModal'
import useFilters from '../../components/FiltersSelector/useFilters'
import { bannedUsersFilters } from '../../lib/filters'

export default function BannedUsersPage() {
  const { modal, closeModal } = useModal()
  const { data: bannedUsers, loading, unauthorized, setData } = useFetchData('get-banned-users', {})
  const { openConfirm, closeConfirm } = useConfirmModal()
  const usersToArray = Object.values(bannedUsers)
  const { array: filteredArray, applyingFilters } = useFilters(bannedUsersFilters, usersToArray)

  function openFiltersModal() {
    const props: ModalOptions = {
      title: 'Filter',
      desc: <FiltersSelector fields={bannedUsersFilters} onAccept={closeModal} />,
      hideDefaultButton: true
    }
    modal(props)
  }

  const onRemoveBan = ({ user_id, username }) => {
    const title = 'Remove ban'
    const desc = `Are you sure you want to unban "${username}"?`
    const onConfirm = () => socket.emit('remove-user-ban', user_id, ({ data }) => {
      closeConfirm()
      setData(data)
    })
    openConfirm({ title, desc, confirmText: 'Remove', onConfirm, waitAfterConfirm: true })
  }

  const filtersButton = (
    <button
      className={`btn-small ${applyingFilters ? 'btn-primary' : 'border'} flex items-center`}
      onClick={openFiltersModal}
    >
      <MdTune size={26} />
      <span className="ml-2">Filter</span>
    </button>
  )

  return (
    <>
      <AdminLayout
        title="Banned users"
        extra={filtersButton}
        unauthorized={unauthorized}
      >
        <List data={filteredArray} loading={loading}>
          {(user: User) => (
            <div key={user.user_id}>
              <div className="px-2 py-6">
                <div className="flex justify-between items-center">
                  <div>
                    <div className="mb-2 font-medium text-lg">
                      {user.username}
                    </div>
                    <div className="md:flex items-center">
                      <div className="text-left opacity-70 md:mr-8">
                        <div className="font-semibold">Until</div>
                        {getDateAndTime(new Date(user.ban.until))}
                      </div>
                      <div className="text-left opacity-70 mt-3 md:mt-0">
                        <div className="font-semibold">Type of ban</div>
                        {typesOfBanParsed[user.ban.type]}
                      </div>
                    </div>
                  </div>
                  <button
                    className="btn-small border"
                    onClick={() => onRemoveBan(user)}
                  >
                    Remove ban
                  </button>
                </div>
              </div>
              <hr />
            </div>
          )}
        </List>
      </AdminLayout>
    </>
  )
}
