import AdminLayout from '../../../containers/AdminLayout'
import { Report } from '../../../lib/types'
import List from '../../../components/List'
import Link from 'next/link'
import useFetchData from '../../../hooks/useFetchData'
import { MdCheck, MdTune } from 'react-icons/md'
import FiltersSelector from '../../../components/FiltersSelector'
import useFilters from '../../../components/FiltersSelector/useFilters'
import { reportsFilters } from '../../../lib/filters'
import { useModal } from '../../../components/Modal'
import { ModalOptions } from '../../../components/Modal/types'
import { getOnlyDate } from '../../../lib/utils'

function RoomLink({ roomName }) {
  return (
    <Link href={`/admin/reports?room_name=${roomName}`}>
      <a className="link">
        {roomName}
      </a>
    </Link>
  )
}

export default function ReportsPage() {

  const { data: reports, unauthorized, loading } = useFetchData('get-reports', {})
  const { modal, closeModal } = useModal()
  const reportsToArray = Object.values(reports)
  const { array: filteredArray, applyingFilters } = useFilters(reportsFilters, reportsToArray)

  function openFiltersModal() {
    const props: ModalOptions = {
      title: 'Filter reports',
      desc: <FiltersSelector fields={reportsFilters} onAccept={closeModal} />,
      hideDefaultButton: true
    }
    modal(props)
  }

  const filtersButton = (
    <button
      className={`btn-small ${applyingFilters ? 'btn-primary' : 'border'} flex items-center`}
      onClick={openFiltersModal}
    >
      <MdTune size={26} />
      <span className="ml-2">Filter reports</span>
    </button>
  )

  return (
    <AdminLayout
      title="Reports"
      unauthorized={unauthorized}
      extra={filtersButton}
    >
      <div className="divide-y divide-solid">
        <List data={filteredArray} loading={loading}>
          {(report: Report, i, currentPage) => (
            <div
              key={report.id}
              className="md:flex items-center py-5"
            >
              <div className="max-w-md mb-3 md:mb-0 flex-1">
                <div className="font-medium">
                  {report.message.username} 
                  <span className="opacity-70">
                    &nbsp;- {getOnlyDate(report.createdAt)} -&nbsp;
                    <RoomLink roomName={report.room.name} />
                  </span>
                </div>
                <div
                  className="my-1 bg-gray-100 dark:bg-gray-800 rounded p-2"
                  dangerouslySetInnerHTML={{__html: report.message.text}}
                />
              </div>
              <div className="ml-auto flex items-center">
                {report.attended && (
                  <div className="p-0.5 rounded text-sm text-white bg-green-600 mr-2">
                    <MdCheck size={16}/>
                  </div>
                )}
                <Link
                  href={`/admin/reports/${report.id}?b=${currentPage}`}
                >
                  <a>
                    <button className="btn-small border">
                      View details
                    </button>
                  </a>
                </Link>
              </div>
            </div>
          )}
        </List>
      </div>
    </AdminLayout>
  )
}
