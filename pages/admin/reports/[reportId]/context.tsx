import MessageContext from '../../../../components/MessageContext'
import ReportDetails from '../../../../containers/ReportDetails'

export default function ReportContext() {
  return (
    <ReportDetails>
      {report => (
        <div className="mt-4">
          <MessageContext
            roomId={report.roomId}
            messageId={report.messageId}
          />
        </div>
      )}
    </ReportDetails>
  )
}
