import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import useFetchData from '../../../../hooks/useFetchData'
import Chat from '../../../../containers/Room/Chat'
import socket from '../../../../lib/socket'
import { User } from '../../../../lib/types'
import Tooltip from '../../../../components/Tooltip'
import Avatar from '../../../../components/Avatar'
import ReportDetails from '../../../../containers/ReportDetails'

export default function Discussion() {

  const router = useRouter()
  const { reportId } = router.query
  const { data, setData } = useFetchData('get-report-discussion', {}, reportId)
  const [creatingRoom, setCreatingRoom] = useState(false)
  const { room, users } = data

  useEffect(() => {
    socket.on('room/add-user', user => {
      setData(({room, users}) => ({
        room,
        users: {
          ...users,
          [user.user_id]: user
        }
      }))
    })

    return () => {
      socket.off('room/add-user')
    }
  }, [])

  const startDiscussion = () => {
    setCreatingRoom(true)
    socket.emit('start-report-discussion', reportId, ({data: room}) => {
      setData(room)
      setCreatingRoom(false)
    })
  }

  const topBar = users && (
    <div className="flex items-center space-x-2 py-3 px-2 border-b flex-wrap">
      {Object.values(users).map((user: User) => (
        <Tooltip text={user.username} key={user.user_id}>
          <Avatar src={user.avatar_url_m}/>
        </Tooltip>
      ))}
    </div>
  )

  return (
    <ReportDetails>
      {() => (
        room ? (
          <div className="h-96">
            <Chat
              roomUsers={users}
              room={room}
              topBar={topBar}
              hideActionsBox
            />
          </div>
        ) : (
          <div className="text-center mt-14">
            <div className="opacity-60 mb-4">
              There isn't a discussion for this report yet
            </div>
            <button
              className="btn border"
              onClick={() => startDiscussion()}
              disabled={creatingRoom}
            >
              {creatingRoom ? 'Starting discussion...' : 'Start discussion'}
            </button>
          </div>
        )
      )}
    </ReportDetails>
  )
}
