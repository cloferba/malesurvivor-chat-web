import { REPORT_TYPES } from '../../../../lib/constants'
import { getDateAndTime } from '../../../../lib/utils'
import Message from '../../../../components/Message'
import ReportDetails from '../../../../containers/ReportDetails'

function Detail({title, children}) {

  return (
    <div className="my-5">
      <div className="font-medium mb-0.5">
        {title}
      </div>
      <div>
        {children}
      </div>
    </div>
  )
}

function ReportAttendedToast({attendedBy, attendedAt}) {
  return (
    <div className="mt-4 px-4 py-0.5 border border-green-300 bg-green-50 rounded-lg">
      <Detail title="Attended at">
        {getDateAndTime(attendedAt)}
      </Detail>

      <Detail title="Attended by">
        {attendedBy}
      </Detail>
    </div>
  )
}

export default function ReportDetail() {
  return (
    <ReportDetails>
      {report => {
        const { label = '', additionalInfo = [] } = REPORT_TYPES[report.type]
        return (
          <>
            {report.attended && <ReportAttendedToast { ...report } />}

            <Detail title="Room">
              {report.room.name}
            </Detail>

            <Detail title="Message">
              <Message {...report.message} />
            </Detail>

            <Detail title="Reason">
              {label}
            </Detail>

            <Detail title="Date of report">
              {getDateAndTime(report.createdAt)}
            </Detail>

            {additionalInfo.map(({name, label}) => (
              <Detail title={label} key={name}>
                {report[name]}
              </Detail>
            ))}
          </>
        )
      }}
    </ReportDetails>
  )
}
