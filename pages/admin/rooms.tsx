import useAddRoomModal from '../../hooks/useAddRoomModal'
import useConfirmModal from '../../components/ConfirmModal'
import List from '../../components/List'
import AdminLayout from '../../containers/AdminLayout'
import useFetchData from '../../hooks/useFetchData'
import socket from '../../lib/socket'
import { Room } from '../../lib/types'
import Link from 'next/link'
import { MdContentCopy, MdDeleteForever, MdEdit, MdOpenInNew } from 'react-icons/md'
import Tag from '../../components/Tag'
import IconButton from '../../components/IconButton'
import Tooltip from '../../components/Tooltip'
import { useState } from 'react'
import { CHAT_URL } from '../../lib/constants'

interface AdminRoomRowProps {
  room: Room,
  onEditRoom: (room) => void,
  onDeleteRoom: (room) => void
}

function AdminRoomRow({ room, onEditRoom, onDeleteRoom }: AdminRoomRowProps) {

  const [copiedTooltip, setCopiedTooltip] = useState(false)

  async function copyUrl(roomId) {
    await navigator.clipboard.writeText(`${CHAT_URL}rooms/${roomId}`)
    setCopiedTooltip(true)
    setTimeout(() => setCopiedTooltip(false), 700)
  }

  return (
    <div className="flex items-center py-2 even:bg-gray-50 dark:even:bg-gray-800 px-2">
      <div className="flex-1 flex items-center">
        {room.name}
        <Link href={`/rooms/${room.id}`}>
          <a className="opacity-60 mt-1 ml-2 hover:opacity-90">
            <MdOpenInNew size={18} />
          </a>
        </Link>
        {room.type == 'link-only' && (
          <Tooltip
            text="Copied"
            visible={copiedTooltip}
          >
            <span
              onClick={() => copyUrl(room.id)}
              className="opacity-60 mt-1 ml-2 hover:opacity-90 cursor-pointer"
            >
              <MdContentCopy size={18} />
            </span>
          </Tooltip>
        )}
      </div>
      <div className="flex-1 flex justify-end items-center ">
        {room.type == 'staff' && <Tag text="Private" />}
        {room.type == 'link-only' && <Tag text="Link only" />}
        {room.disabled && <Tag text="Disabled" className="bg-red-900 text-white" />}
        <IconButton
          className="opacity-60 hover:opacity-90"
          onClick={() => onEditRoom(room)}
          icon={MdEdit}
        />
        <IconButton
          className="opacity-60 hover:opacity-90 hover:text-red-800"
          onClick={() => onDeleteRoom(room)}
          icon={MdDeleteForever}
        />
      </div>
    </div>
  )
}

export default function AdminRooms() {

  const { data: rooms, loading, setData, unauthorized } = useFetchData('admin-get-rooms', {})
  const { openConfirm, closeConfirm } = useConfirmModal()
  const openAddRoomModal = useAddRoomModal(setData)

  const addRoomButton = (
    <button
      className="btn btn-primary"
      onClick={() => openAddRoomModal()}
    >
      Add room
    </button>
  )

  const deleteRoom = (roomId) => {
    socket.emit('delete-room', roomId, () => {
      closeConfirm()
      setData(currentRooms => {
        let result = {...currentRooms}
        delete result[roomId]
        return result
      })
    })
  }

  const deleteRoomClicked = ({name, id}) => {
    const actionLabel = 'Remove'
    const title = `${actionLabel} room`
    const desc = `${actionLabel} room "${name}"? The messages of this room won't be deleted.`
    const onConfirm = () => deleteRoom(id)
    openConfirm({title, desc, confirmText: actionLabel, onConfirm, waitAfterConfirm: true })
  }

  return (
    <AdminLayout
      title="Rooms"
      unauthorized={unauthorized}
      extra={addRoomButton}
    >
      <h4 className="flex py-2 px-2">
        <div className="flex-1">
          Name
        </div>
        <div className="flex-1 text-right" />
      </h4>
      <List
        data={Object.values(rooms)}
        loading={loading}
      >
        {(room: Room) => (
          <AdminRoomRow
            key={room.id}
            room={room}
            onEditRoom={openAddRoomModal}
            onDeleteRoom={deleteRoomClicked}
          />
        )}
      </List>
    </AdminLayout>
  )
}
