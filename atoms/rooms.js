import { atom } from 'recoil'

const roomsState = atom({
  key: 'rooms',
  default: {},
})
export default roomsState