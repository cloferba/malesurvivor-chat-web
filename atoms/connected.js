import { atom } from 'recoil'

const connectedState = atom({
  key: 'connected',
  default: false,
})
export default connectedState