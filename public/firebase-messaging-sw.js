importScripts('https://www.gstatic.com/firebasejs/8.4.3/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/8.4.3/firebase-messaging.js')

firebase.initializeApp({
  apiKey: 'AIzaSyCLhuZ0fr_EBYLkTObXGGvPE1TZo7_GIbY',
  authDomain: 'malesurvivor-chat.firebaseapp.com',
  projectId: 'malesurvivor-chat',
  storageBucket: 'malesurvivor-chat.appspot.com',
  messagingSenderId: '208982389415',
  appId: '1:208982389415:web:13dbe7a090be288c5da182'
})

const messaging = firebase.messaging()